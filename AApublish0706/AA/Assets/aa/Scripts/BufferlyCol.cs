﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AppAdvisory.AA
{
public class BufferlyCol : MonoBehaviour {
		GameManager _gameManager;
		GameManager gameManager
		{
			get
			{
				if(_gameManager == null)
					_gameManager = FindObjectOfType<GameManager>();

				return _gameManager;
			}
		}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
		void OnCollisionEnter2D(Collision2D col)
		{
			GameOverLogic (col.gameObject);
		}

		void OnCollisionStay2D(Collision2D col)
		{
			GameOverLogic (col.gameObject);
		}

		void OnTriggerEnter2D(Collider2D col)
		{
			GameOverLogic (col.gameObject);
		}

		void OnTriggerStay2D(Collider2D col) 
		{
			GameOverLogic (col.gameObject);
		}

		void GameOverLogic(GameObject col)
		{
			if( !gameManager.isGameOver &&  col.name.Contains("Dot"))
			{
				GetComponent<Collider2D> ().enabled = false;
				transform.parent = null;
				col.GetComponent<Collider2D> ().enabled = false;

				gameManager.AnimationCameraGameOver ();
			}

		}
}
}