﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
using Facebook.MiniJSON;

namespace AppAdvisory.AA
{
public class FBScript : MonoBehaviour {
	public GameObject DialogLoggedin;
	public GameObject DialogLoggedOut;
	public GameObject DialogUsername;
	public GameObject DialogProfilepic;
	public Button FBLogout;
	public Text ScoresDebug;
    Image temp_user;
	public GameObject ScoreEntryPanel;
	public GameObject ScrollScoreList;
	private static string applink = "https://fb.me/518443178503747";

	void Awake(){
		
		if (!FB.IsInitialized) {
			// Initialize the Facebook SDK
			FB.Init(InitCallback, OnHideUnity);
		} else {
			// Already initialized, signal an app activation App Event
			FB.ActivateApp();
		}
	}


	void Start () {
		Button btnFBlogout = FBLogout.GetComponent<Button> ();
		btnFBlogout.onClick.AddListener (OnClickedFBlogout);
	}

	void OnClickedFBlogout()
	{
		if (FB.IsLoggedIn) {
			FB.LogOut ();
		}
		DealWithFBMenus (FB.IsLoggedIn);
	}

	private void InitCallback ()
	{
		if (FB.IsInitialized) {
			// Signal an app activation App Event
			FB.ActivateApp();
			// Continue with Facebook SDK
			// ...
		} else {
			Debug.Log("Failed to Initialize the Facebook SDK");
		}
	}

	private void OnHideUnity (bool isGameShown)
	{
		if (!isGameShown) {
			// Pause the game - we will need to hide
			Time.timeScale = 0;
		} else {
			// Resume the game - we're getting focus again
			Time.timeScale = 1;
		}
	}

	public void FBlogin(){
		var perms = new List<string>(){"public_profile", "email", "user_friends"};
		FB.LogInWithReadPermissions(perms, AuthCallback);
	}

	private void AuthCallback (ILoginResult result) {
		if (FB.IsLoggedIn) {
			// AccessToken class will have session details
			var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
			// Print current access token's User ID
			Debug.Log(aToken.UserId);
			// Print current access token's granted permissions
			foreach (string perm in aToken.Permissions) {
				Debug.Log(perm);
			}
		} else {
			Debug.Log("User cancelled login");
		}
		DealWithFBMenus (FB.IsLoggedIn);

    }

	void DealWithFBMenus (bool isLoggedIn){
		if (isLoggedIn) {
			DialogLoggedin.SetActive (true);
			DialogLoggedOut.SetActive (false);
			FB.API ("/me?fields=first_name", HttpMethod.GET, DisplayUsername);
			FB.API("/me/picture?type=square&height=128&width=128", HttpMethod.GET, DisplayProfilepic);
		} else {
			DialogLoggedin.SetActive (false);
			DialogLoggedOut.SetActive (true);
		}
	}
	void DisplayUsername(IGraphResult result){
		Text Username = DialogUsername.GetComponent <Text>();
		if(string.IsNullOrEmpty(result.Error)){
			Username.text = "Hi, " + result.ResultDictionary ["first_name"];
			print (Username.text.ToString());
		} else {
			Debug.Log (result.Error);
			print ("reslut error");
		}
	}	
	void DisplayProfilepic(IGraphResult result){
		if(string.IsNullOrEmpty(result.Error)){
			string photoURL = "https" + "://graph.facebook.com/" + Facebook.Unity.AccessToken.CurrentAccessToken.UserId + "/picture?type=large";
			StartCoroutine(fetchProfilePic(photoURL));
		} else {
			Debug.Log (result.Error);
			print ("reslut error");
		}
	}
	private IEnumerator fetchProfilePic (string url) {
		WWW www = new WWW(url);
		yield return www;
		Image Profilepic = DialogProfilepic.GetComponent <Image> ();
		Profilepic.sprite = Sprite.Create (www.texture, new Rect (0,0,128,128), new Vector2());
	}

	public void Invite(){
		if (FB.IsLoggedIn) {
		FB.Mobile.AppInvite (new System.Uri(applink), null, InviteCallback);
		}
	}
	void InviteCallback (IAppInviteResult result){
		if (result.Cancelled) {
			Debug.Log ("Invite cancelled");
		} else if (string.IsNullOrEmpty(result.Error)){
			Debug.Log ("Invite was successful" + result.RawResult);
		}
	}

	public void QueryScores(){
		FB.API ("/app/scores?fields=score,user.limit(20)",HttpMethod.GET, ScoresCallback);
	}

	private void ScoresCallback (IGraphResult result){
	
		UnityEngine.Debug.Log(result.RawResult);

		var dataList = result.ResultDictionary["data"] as List<object>;

		foreach (Transform child in ScrollScoreList.transform){
			GameObject.Destroy (child.gameObject);
		}

		foreach (var Score in dataList ){
			var entry = Score as Dictionary<string, object>;
			var user = entry["user"] as Dictionary<string, object>;
			GameObject ScorePanel = Instantiate (ScoreEntryPanel) as GameObject;
			ScorePanel.transform.parent = ScrollScoreList.transform;
			Transform ThisScoreName = ScorePanel.transform.Find ("FriendName");
			Transform ThisScoreScore = ScorePanel.transform.Find ("FriendScore");
			Text ScoreName = ThisScoreName.GetComponent<Text> ();
			Text ScoreScore = ThisScoreScore.GetComponent<Text> ();
			ScoreName.text = user ["name"].ToString ();
			ScoreScore.text = entry ["score"].ToString ();
			string user_id = user ["id"].ToString ();;
			Transform TheUserAvatar = ScorePanel.transform.Find ("FriendAvatar");
	//		Image UserAvatar = TheUserAvatar.GetComponent<Image> ();
			temp_user = TheUserAvatar.GetComponent<Image> ();
			FB.API("/user_id/picture?type=square&height=128&width=128", HttpMethod.GET, DisplayProfilepicuser);
	    }
	}

		void DisplayProfilepicuser(IGraphResult result){
			if(string.IsNullOrEmpty(result.Error)){
				string photoURL = "https" + "://graph.facebook.com/" + Facebook.Unity.AccessToken.CurrentAccessToken.UserId + "/picture?type=large";
				StartCoroutine(fetchProfilePicuser(photoURL));
			} else {
				Debug.Log (result.Error);
				print ("reslut error");
			}
		}

		private IEnumerator fetchProfilePicuser (string url) {
			WWW www = new WWW(url);
			yield return www;
			temp_user.sprite = Sprite.Create (www.texture, new Rect (0,0,128,128), new Vector2());
		}

	public void SetScore(string _score){
		var scoreData = new Dictionary <string, string> ();
		scoreData ["score"] = _score;
		FB.API ("/me/scores", HttpMethod.POST, delegate(IGraphResult result){
			Debug.Log("Score submit result: " + result.RawResult);}, scoreData);
	}
}
}