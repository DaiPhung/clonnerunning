﻿
/***********************************************************************************************************
 * Produced by App Advisory	- http://app-advisory.com													   *
 * Facebook: https://facebook.com/appadvisory															   *
 * Contact us: https://appadvisory.zendesk.com/hc/en-us/requests/new									   *
 * App Advisory Unity Asset Store catalog: http://u3d.as/9cs											   *
 * Developed by Gilbert Anthony Barouch - https://www.linkedin.com/in/ganbarouch                           *
 ***********************************************************************************************************/

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
#if AADOTWEEN
using DG.Tweening;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Facebook.Unity;
using Facebook.MiniJSON;


#if APPADVISORY_LEADERBOARD
using AppAdvisory.social;
#endif
#if APPADVISORY_ADS
using AppAdvisory.Ads;
#endif
#if VS_SHARE
using AppAdvisory.SharingSystem;
#endif

namespace AppAdvisory.AA
{
	/// <summary>
	/// Class in charge of the game logic.
	/// </summary>
	public class GameManager : MonobehaviourHelper
	{
		public int numberOfPlayToShowInterstitial = 5;

		public string VerySimpleAdsURL = "";


		public GameObject LeaderboardManagerPrefab;
		/// <summary>
		/// Event triggered when player successfully cleared a level
		/// </summary>
		public delegate void SuccessStart();
		public static event SuccessStart OnSuccessStart;

		/// <summary>
		/// Event triggered when player successfully cleared a level and the animation success is completed
		/// </summary>
		public delegate void SuccessComplete();
		public static event SuccessComplete OnSuccessComplete;

		/// <summary>
		/// Event triggered when player failed to clear a level
		/// </summary>
		public delegate void FailStart();
		public static event FailStart OnFailStart;

		/// <summary>
		/// Event triggered when player failed to clear a level and the animation success is completed
		/// </summary>
		public delegate void FailComplete();
		public static event FailComplete OnFailComplete;
		public GameObject FBholder;

		public delegate void Updatelist(int level);
		public static event Updatelist OnUpdatelist;

		public delegate void Updatelevel(int level);
		public static event Updatelevel OnUpdatelevel;
		/// <summary>
		/// Text displayed in the main circle in the center of the screen
		/// </summary>
		public Text textNumLevel;
		public Text textStars;
		public Text TextAchievement;

		private float _timecounter;
		private int _countcombo = 0;
		public bool _comboX3 = false;
		public bool _comboX5 = false;
		public bool flag = true;
		private bool firsttime = false;
		public float timeleft;
		public float timeAchie = 0f;
		public float timecombo;
		int _level;
		/// <summary>
		/// Reference to the current level played, and modify the textNumLevel
		/// </summary>
		public int Level
		{
			get
			{
				return _level;
			}
			set
			{
				_level = value;

				if (textNumLevel == null)
					textNumLevel = CircleTransform.GetComponentInChildren<Text> ();

				textNumLevel.text = value.ToString ();
			}
		}

		int maxLevel
		{
			get 
			{
				return PlayerPrefs.GetInt (Constant.LEVEL_UNLOCKED, 1);
			}
		}

		/// <summary>
		/// true if player can shoot a dot, false if not
		/// </summary>
		public bool playerCanShoot;
		/// <summary>
		/// true if player sucessfully cleared a level
		/// </summary>
		public bool isSuccess;
		/// <summary>
		/// true if level is completed (success or not)
		/// </summary>
		public bool isGameOver;

		float sizeDot = 0;

		/// <summary>
		/// reference to the center circle of the game
		/// </summary>
		public Transform CircleTransform;
		/// <summary>
		/// reference to the parent transform of all dots linked to the center circle
		/// </summary>
		public Transform CircleBorder;
		public Transform CircleDot;
		public Transform CircleLine;
		public GameObject IntrosceneGo;
		public GameObject StarmoveX5;
		public GameObject StarmoveX3;
		public GameObject StarmoveOntime;
		public Transform StarsDestination;
		public ParticleSystem FairyDust;
		/// <summary>
		/// list of all the dots the player have to shoot in the level
		/// </summary>
		List<DotManager> Dots;

		[System.Serializable]
		public class BgColor
		{
			public GameObject _BgColor;
		}
		public List<BgColor> BgColorList;

		private float positionTouchBorder
		{
			get
			{
				var height = 2f * Camera.main.orthographicSize;
				var width = height * Camera.main.aspect;
				return Mathf.Min(width,height) * 1f / 4f * 0.5f * LEVEL.lineRadius;
			}
		}


		public GameObject DotPrefab;
		public GameObject FlowerPrefab;
		public GameObject ComboIris;
		public GameObject ComboHisbicus;
		public GameObject ComboColumbine;
		public GameObject ComboPlumeria;
		public GameObject ComboLotus;
		public GameObject ComboRose;
		public GameObject ComboRoseYe;
		public GameObject Bufferly;
		public GameObject Beefly;

		public GameObject Sakura;
		public GameObject Iris;
		public GameObject Hisbicus;
		public GameObject Columbine;
		public GameObject Plumeria;
		public GameObject Lotus;
		public GameObject Rose;
		public GameObject RoseYellow;

		public List<GameObject> poolODDT;

		private Animator anim;
		public Animator anim_ontime;
		public Animator anim_comboX3;
		public Animator anim_comboX5;
		private bool stop_ontime = true;
		/// <summary>
		/// Spawn the dot prefab from the ObjectPooling. 
		/// </summary>
		public GameObject SpawnDotPrefab()
		{
			var o = ObjectPoolingManager.Instance.GetObject (DotPrefab.name);
			o.transform.parent = CircleTransform;
			return o;
		}



		/// <summary>
		/// Spawn the dot prefabs from the ObjectPooling. 
		/// </summary>
		void PreparePoolDots()
		{

			ObjectPoolingManager.Instance.CreatePool(this.DotPrefab, 25, 30);

			var allDots = ObjectPoolingManager.Instance.GetObjects (DotPrefab.name);


			foreach (var d in allDots) 
			{
				d.SetActive (true);
				d.transform.rotation = Quaternion.identity;
				d.GetComponent<DotManager>().SetScale();
				d.GetComponent<DotManager> ().ActivateLine ();

				d.transform.parent = transform;


				d.transform.position = new Vector3 (0, Screen.height * 2, 0);


				d.SetActive (false);
			}
		}

		/// <summary>
		/// Disable all spawned object 
		/// </summary>
		public void DespawnAll()
		{
			
				var allDots = ObjectPoolingManager.Instance.GetObjects (DotPrefab.name);

				foreach (var d in allDots) {
					if (d.activeInHierarchy) {
						d.transform.rotation = Quaternion.identity;
						d.GetComponent<DotManager> ().SetScale ();
						d.GetComponent<DotManager> ().DesactivateLine ();

						d.transform.parent = CircleTransform;
						d.SetActive (false);
					}
				}
			

		}

		/// <summary>
		/// Do it at first. Some configurations.
		/// </summary>
		void Awake()
		{
			firsttime = true;
		
			if(Time.realtimeSinceStartup < 20)
			{
				DOTween.Init ();
			}

			CircleTransform = GameObject.Find("Circle").transform.Find("SpriteCircleCenter");
			CircleBorder = GameObject.Find("Circle").transform.Find("Border");
			CircleDot = GameObject.Find("Circle").transform.Find("CircleDot");
			//		PlayerPrefs.DeleteKey ("LEVEL");
			if (!PlayerPrefs.HasKey ("LEVEL")) {
				PlayerPrefs.SetInt ("LEVEL", 1);
			} 
				
			if (!PlayerPrefs.HasKey ("LevelNotPass")) {
				PlayerPrefs.SetInt ("LevelNotPass", 1);
			}
			if (PlayerPrefs.GetInt ("LEVEL") <= 0) {
				PlayerPrefs.SetInt ("LEVEL", 1);
			}

			if (!PlayerPrefs.HasKey ("LEVEL_PLAYED")) {
				PlayerPrefs.SetInt ("LEVEL_PLAYED", 1);
			}  

			if (PlayerPrefs.GetInt ("LEVEL_PLAYED") <= 0) {
				PlayerPrefs.SetInt ("LEVEL_PLAYED", 1);
			}
			if (!PlayerPrefs.HasKey ("STARS")) {
				PlayerPrefs.SetInt ("STARS", 0);
			} 
			if (!PlayerPrefs.HasKey ("Achie_10s")) {
				PlayerPrefs.SetInt ("Achie_10s", 0);
			} 
				
			isGameOver = true;
			anim = FlowerPrefab.GetComponent<Animator> ();
			Dots = new List<DotManager>();

			Camera.main.transform.position = new Vector3 (0, 2f, -10);

			PreparePoolDots ();
			FindObjectOfType<InputTouch>().BLOCK_INPUT = true;

		}
			
		void PlayAnim(){
			if (this.Level <= 100) {
				anim = FlowerPrefab.GetComponent<Animator> ();
			} else if (this.Level <= 200) {
				anim = ComboIris.GetComponent<Animator> ();
			} else if (this.Level <= 250) {
				anim = ComboHisbicus.GetComponent<Animator> ();
			} else if (this.Level <= 300) {
				anim = ComboColumbine.GetComponent<Animator> ();
			} else if (this.Level <= 400) {
				anim = ComboPlumeria.GetComponent<Animator> ();
			} else if (this.Level <= 500) {
				anim = ComboLotus.GetComponent<Animator> ();
			} else if (this.Level <= 550) {
				anim = ComboRose.GetComponent<Animator> ();
			} else if (this.Level <= 600) {
				anim = ComboRoseYe.GetComponent<Animator> ();
			}
		}
		void Start()
		{
			CircleTransform.GetComponentInChildren<SpriteRenderer>().color = constant.DotColor;
			MyAdMob.Instance.ShowBanner ();
			FairyDust.Stop ();
			CreateGame (PlayerPrefs.GetInt (Constant.LAST_LEVEL_PLAYED, 1));	
					}
		/// <summary>
		/// Add all listeners 
		/// </summary>
		void OnEnable()
		{
			InputTouch.OnTouchScreen += OnTouch;
			CanvasManager.OnCreateGame += CreateGame;
		}

		/// <summary>
		/// Remove all listeners 
		/// </summary>
		void OnDisable()
		{
			InputTouch.OnTouchScreen -= OnTouch;
			CanvasManager.OnCreateGame -= CreateGame;
		}


		/// <summary>
		/// Player has shown rewarded video, unlock this level
		/// </summary>
		void OnReawardedVideoSuccess(bool success)
		{
			if(success)
			{
				AnimationCameraSuccess();
			}
			else
			{
				print("the user dismiss the video...");
			}
		}


		/// <summary>
		/// Function called by InputTouch delegate. Do the shoot of the dot.
		/// </summary>
		void OnTouch(Vector2 touchPos)
		{
		//	if (introMenu == null || introMenu.gameObject.activeInHierarchy)
		//		return;

			if ((Input.mousePosition.y < Screen.height * 0.6f) && (Input.mousePosition.x < Screen.width * 0.9f)) 
			{
				if (!isGameOver && playerCanShoot) {
					if (Dots == null) {
						return;
					}

				//	if (Dots.Count > 0 && (PlayerPrefs.GetInt ("ShotDot",1)==1)) {
					if (Dots.Count > 0){
						ShootDot (Dots [0]);
					}
				} 
			}
		}

		/// <summary>
		/// Keep a reference of the Dotween sequtimence use to rotate the circle and the dots linked to the circle
		/// </summary>
		Sequence sequence;

		/// <summary>
		/// Rotate the circle and the dots linked to it
		/// </summary>
		void LaunchRotateCircle()
		{
			if (sequence != null)
				sequence.Kill (false);

			sequence = DOTween.Sequence ();

			if(constant.AnimLineAtEachTurn)
			{
				var listToAnim = CircleBorder.gameObject.GetComponentsInChildren<DotManager> ();

				sequence.Append (DOVirtual.Float (Constant.LINE_WIDHT, Constant.LINE_WIDHT * 3, 0.2f, (float v) => {

					foreach(var d in listToAnim)
					{
						d.SetLineScale(v);
					}

				}).SetLoops (6, LoopType.Yoyo));
			}
			if (LEVEL.rotateLoopType == LoopType.Incremental) {
				sequence.Append (CircleBorder.DORotate (LEVEL.rotateVector * UnityEngine.Random.Range (360, 500), LEVEL.rotateDelay, RotateMode.FastBeyond360)
					.SetEase (LEVEL.rotateEase));
				sequence.SetLoops (1, LEVEL.rotateLoopType);
			} else {
				sequence.Append (CircleBorder.DORotate (LEVEL.rotateVector * UnityEngine.Random.Range (360, 500), LEVEL.rotateDelay, RotateMode.FastBeyond360)
					.SetEase (LEVEL.rotateEase));
				sequence.SetLoops (2, LEVEL.rotateLoopType);
			}

			sequence.Insert (0,CircleDot.DORotate (LEVEL.rotateVector * UnityEngine.Random.Range (180, 450), LEVEL.rotateDelay, RotateMode.FastBeyond360));
			sequence.OnStepComplete (LaunchRotateCircle);
			sequence.Play ();
		}

		/// <summary>
		///  Keep a reference of the LEVEL use to generate the level
		/// </summary>
		Level LEVEL;


		/// <summary>
		/// Method to create the level
		/// </summary>
		public void CreateGame(int level)
		{
			starmovefinish ();
			ReportScoreToLeaderboard(level);
			if (FB.IsLoggedIn) {
				FBholder.GetComponent<FBScript> ().SetScore (level.ToString ());
			}
			playerCanShoot = false;
			isGameOver = true;
			FindObjectOfType<InputTouch>().BLOCK_INPUT = false;
			if (IntrosceneGo.GetComponent<CanvasGroup>().alpha == 1){
				FindObjectOfType<InputTouch>().BLOCK_INPUT = true;
			}
			Application.targetFrameRate = 60;

			GC.Collect ();

			this.Level = level;


			StopAllCoroutines ();
			ThemeManager ();
			BackgroundManager ();
			PlayAnim ();
			DespawnAll ();

			textStars.text = PlayerPrefs.GetInt ("STARS", 0).ToString();

			Dots = new List<DotManager>();

			this.LEVEL = levelManager.GetLevel (Level);
	
			timeleft = this.LEVEL.timeleft;
			timeAchie = timeleft;
			timecombo = this.LEVEL.timecombo;
			CheckFirst ();
			CheckBee ();
			CheckBufferly ();
			if (OnUpdatelevel != null) {
				OnUpdatelevel (this.Level);
			}
			CircleBorder.localScale = new Vector3 (1, 1, 1);

			CreateDotOnCircle ();
			CreateListDots ();

			PositioningDotsToShoot();

			LaunchRotateCircle ();

			isSuccess = false;
			isGameOver = false;
			playerCanShoot = true;
			#if VS_SHARE
			VSSHARE.DOOpenScreenshotButton();
			#endif
		}

		void BackgroundManager(){
			
			if (this.Level <= 100) {
				BgColorList [0]._BgColor.SetActive (false);
				BgColorList [1]._BgColor.SetActive (false);
				BgColorList [2]._BgColor.SetActive (false);
				Camera.main.backgroundColor = constant.BackgroundColor;
			} else if (this.Level <= 300) {
				BgColorList [0]._BgColor.SetActive (true);
				BgColorList [1]._BgColor.SetActive (false);
				BgColorList [2]._BgColor.SetActive (false);
				Camera.main.backgroundColor = constant.Background1;
			}else if (this.Level <= 450) {
				BgColorList [0]._BgColor.SetActive (false);
				BgColorList [1]._BgColor.SetActive (true);
				BgColorList [2]._BgColor.SetActive (false);
				Camera.main.backgroundColor = constant.Background2;
			}else if (this.Level <= 600) {
				BgColorList [0]._BgColor.SetActive (false);
				BgColorList [1]._BgColor.SetActive (false);
				BgColorList [2]._BgColor.SetActive (true);
				Camera.main.backgroundColor = constant.Background3;
			}
		}
		void ThemeManager(){
			Sakura.SetActive (false);
			Iris.SetActive (false);
			Hisbicus.SetActive (false);
			Columbine.SetActive (false);
			Plumeria.SetActive (false);
			Lotus.SetActive (false);
			Rose.SetActive (false);
			RoseYellow.SetActive (false);
			if (this.Level <= 100) {
				Sakura.SetActive (true);
			} else if (this.Level <= 200) {
				Iris.SetActive (true);
			} else if (this.Level <= 250) {
				Hisbicus.SetActive (true);
			} else if (this.Level <= 300) {
				Columbine.SetActive (true);
			} else if (this.Level <= 400) {
				Plumeria.SetActive (true);
			} else if (this.Level <= 500) {
				Lotus.SetActive (true);
			} else if (this.Level <= 550) {
				Rose.SetActive (true);
			} else if (this.Level <= 600) {
				RoseYellow.SetActive (true);
			}
		}
		void CheckBufferly(){
			if (this.Level > 400 && this.Level <= 600){
				Bufferly.SetActive (true);
				Bufferly.GetComponent<Collider2D> ().enabled = true;
				Bufferly.transform.parent = CircleDot;
				Bufferly.transform.position = new Vector3 (0, +positionTouchBorder + 15f + positionTouchBorder*(1f-LEVEL.lineRadius), 0);
				if (!LEVEL.rotateRight) {
					Bufferly.transform.rotation = Quaternion.AngleAxis (90, Vector3.back);
				} else {
					Bufferly.transform.rotation = Quaternion.AngleAxis (90, Vector3.forward);
				}

			}else {
				Bufferly.SetActive (false);
			}
		}

		void CheckBee(){
			if ((this.Level > 300 && this.Level <= 400) || this.Level >500) {
				Beefly.SetActive (true);
				Beefly.GetComponent<Collider2D> ().enabled = true;
				Vector3 temp = Beefly.transform.position;
				temp.x = positionTouchBorder + 2.5f;
				Beefly.transform.position = temp;
			} else {
				Beefly.SetActive (false);
			}

		}
		/// <summary>
		/// check combox3, combox5, ontime
		/// </summary>
		void CheckFirst(){
			if (!PlayerPrefs.HasKey ("Level" + this.LEVEL.levelNumber + "Ontime")) {
				PlayerPrefs.SetInt ("Level" + this.LEVEL.levelNumber + "Ontime", 0);
			}
			if (!PlayerPrefs.HasKey ("Level" + this.LEVEL.levelNumber + "ComboX3")) {
				PlayerPrefs.SetInt ("Level" + this.LEVEL.levelNumber + "ComboX3", 0);
			} 
			if (!PlayerPrefs.HasKey ("Level" + this.LEVEL.levelNumber + "ComboX5")) {
				PlayerPrefs.SetInt ("Level" + this.LEVEL.levelNumber + "ComboX5", 0);
			} 

			if (PlayerPrefs.GetInt ("Level" + this.LEVEL.levelNumber + "Ontime") == 1) {
				stop_ontime = true;
				anim_ontime.Play ("Idle",-1,0f);
				textNumLevel.gameObject.SetActive (false);
			} else {
				textNumLevel.gameObject.SetActive (true);
				anim_ontime.ResetTrigger ("OntimeFad");
				anim_ontime.Play ("Flowering",-1,0f);
				stop_ontime = false;
			} 

			if (PlayerPrefs.GetInt ("Level" + this.LEVEL.levelNumber + "ComboX3") == 1) {
				_comboX3 = true;
				anim_comboX3.Play ("Idle", -1, 0f);
			} else {
				_comboX3 = false;
				anim_comboX3.ResetTrigger ("ComboX3Flo");
				anim_comboX3.Play ("Bud",-1,0f);
			}

			if (PlayerPrefs.GetInt ("Level" + this.LEVEL.levelNumber + "ComboX5") == 1) {
				_comboX5 = true;
				anim_comboX5.Play ("Idle", -1, 0f);
			} else {
				_comboX5 = false;
				anim_comboX5.ResetTrigger ("ComboX5Flo");
				anim_comboX5.Play ("Bud",-1,0f);
			}
		}

		/// <summary>
		/// Create the dots on the circle and activate the line to link the dots to the circle
		/// </summary>
		void CreateDotOnCircle()
		{
		//	float temp = 1f;
		//	temp = temp - LEVEL.lineRadius + 0.1f;
			for (int i = 0; i < LEVEL.numberDotsOnCircle ; i++)
			{
				CircleBorder.rotation = Quaternion.Euler( new Vector3 (0, 0, ((float)i) * 360f / LEVEL.numberDotsOnCircle) );

				Transform t = SpawnDotPrefab().transform;

			//	t.position = new Vector3 (0, -positionTouchBorder, 0);
				t.parent = CircleBorder;
				t.rotation = Quaternion.identity;

				t.gameObject.SetActive (true);

				DotManager dm = t.GetComponent<DotManager> ();
				if (dm.DotSprite.color == dm.color2) {
					t.position = new Vector3 (0, -positionTouchBorder + 4.7f - positionTouchBorder*(1f-LEVEL.lineRadius), 0);
				} else {
					t.position = new Vector3 (0, -positionTouchBorder + 1f - positionTouchBorder*(1f-LEVEL.lineRadius), 0);
				};
				t.GetComponent<Collider2D>().enabled = true;
				dm.ActivateLine ();

				dm.ActivateNum (false);
			}
		}
			
		void Update(){
			if (!stop_ontime) {
				CheckOntime ();
			}
			if (_comboX3 == true && _comboX5 == true)
				return;
			Combocounter();
		}

		/// <summary>
		/// check when finish timer
		/// </summary>
		void CheckOntime(){
			if (playerCanShoot) {
				if (timeleft > 0) {
					timeleft -= Time.deltaTime;
				} else {
					anim_ontime.Play ("Fading");
					stop_ontime = true;
					textNumLevel.gameObject.SetActive (false);
					StartCoroutine(GameOver(2f));
				}
			}
			textNumLevel.text = ((int)timeleft).ToString();
		}

		IEnumerator GameOver(float seconds)
		{
			yield return new WaitForSeconds(seconds);
			AnimationCameraGameOver ();
		}

		/// <summary>
		/// domovestar
		/// </summary>
		void DomoveStar (GameObject GameOjStar, float time){   
			GameOjStar.gameObject.SetActive (true);
			Transform temp_position = GameOjStar.transform;
			GameOjStar.transform.position = new Vector3 (temp_position.position.x,temp_position.position.y,-1); 
			GameOjStar.transform.DOMove ( StarsDestination.position, time );
			GameOjStar.transform.DOScale (Vector3.one * 2f, time);
			GameOjStar.transform.DORotate (LEVEL.rotateVector * UnityEngine.Random.Range (360, 500), time, RotateMode.FastBeyond360).OnComplete (() => {
				GameOjStar.transform.position = GameOjStar.transform.parent.position;
				GameOjStar.gameObject.SetActive (false);
				int temp = Int32.Parse (textStars.text) + 1;
				textStars.text = temp.ToString();
			});
			StartCoroutine ("Waitfinish");
		}

		IEnumerator Waitfinish()
		{
			yield return null;
			yield return new WaitForSeconds (4f);
		}
		/// <summary>
		/// count combo on each level
		/// </summary>
		void Combocounter(){
			if (playerCanShoot == false && flag ==true){
				if (_timecounter < timecombo) {
					_countcombo += 1;
					if (_comboX5 == false) {
						if (_countcombo == 5) {
							anim.SetBool ("combo", true);
							_comboX5 = true;
							anim_comboX5.Play ("Flowering");
							DomoveStar (StarmoveX5, 3f);
						}
					}
					if (_comboX3 == false) {
						if (_countcombo == 3){
							anim.SetBool ("combo", true);
							_comboX3 = true;	
							anim_comboX3.Play ("Flowering");
							StarmoveX3.gameObject.SetActive (true);
							DomoveStar (StarmoveX3, 3f);
						}
					}
				} else {
					_countcombo = 1;
					anim.SetBool ("combo", false);
				}
				if (_countcombo > 5) {
					anim.SetBool ("combo", false);
					_countcombo = 1;
				}
				flag = false;
				_timecounter = 0f;
			} else{
				anim.SetBool ("combo", false);
				_timecounter += Time.deltaTime;
			}
		}

		/// <summary>
		/// Create the dots on the circle and activate the line to link the dots to the circle
		/// </summary>
		/// 
		void CreateListDots(){

			for (int i = 0; i < LEVEL.numberDotsToCreate; i++) {

				DotManager dm = SpawnDotPrefab ().GetComponent<DotManager> ();

				dm.SetNum (i + 1);
			

				dm.ActivateNum (true);

				dm.GetComponent<Collider2D>().enabled = false;

				dm.transform.parent = CircleTransform;

				if (sizeDot == 0) 
				{
					sizeDot = dm.DotSprite.bounds.size.x * 1.1f;
				}

				Dots.Add (dm);
			}
		}


		/// <summary>
		/// Method to shoot the first dot and moving the other. This method check if the list of dots to shoot is empty or not. If the list is empty, this method triggered the success for this level.
		/// </summary>
		void ShootDot(DotManager d)
		{
			playerCanShoot = false;
			flag = true;
			StopCoroutine("PositioningDots");

			StopCoroutine("MoveStartPositionDot");

			d.GetComponent<Collider2D>().enabled = true;

			soundManager.PlaySoundBeep ();

			Dots.Remove (d);

			PositioningDotsToShoot();

			d.GetComponent<Collider2D>().enabled = true;

			d.transform.position = new Vector3 (0, -positionTouchBorder + (-0 - 2) * sizeDot, 0);

			d.transform.DOKill();
			float temp = positionTouchBorder;
			if (d.DotSprite.color == d.color2){temp = positionTouchBorder - 4.7f + positionTouchBorder*(1f-LEVEL.lineRadius);
			}else{
				temp = positionTouchBorder - 1f + positionTouchBorder*(1f-LEVEL.lineRadius);
			}

			d.transform.DOMoveY (-temp, 0.1f).SetEase(Ease.Linear)
				.OnUpdate (() => {
					playerCanShoot = false;
					if(isGameOver)
						DOTween.Kill(d.transform);
				})
				.OnComplete (() => {
					d.ActivateLine ();

					d.transform.parent = CircleBorder;

					playerCanShoot = true;

					if (Dots.Count == 0 && !isGameOver)
						isSuccess = true;

					if (isSuccess && !isGameOver)
					{
						AnimationCameraSuccess();
					}

					PositioningDotsToShoot();

				});

			#if VS_SHARE
			VSSHARE.DOHideScreenshotIcon();
			#endif
		}

		/// <summary>
		/// Move the list of the dots to shoot when a dot is shooted.
		/// </summary>
		void PositioningDotsToShoot()
		{
			for (int i = 0; i < Dots.Count; i++) 
			{
				if (Dots.Count > 0) 
				{
					Dots [i].transform.DOKill();

					Dots [i].SetScale();
					Dots [i].transform.DOMove (new Vector3 (0, -positionTouchBorder - 3 + (-i - 2) * sizeDot ), 0.2f );
					Dots [i].GetComponent<Collider2D>().enabled = false;
				}
			}
		}

		/// <summary>
		/// Clean memory if the game is pausing
		/// </summary>
		public void OnApplicationPause(bool pause)
		{
			if (!pause)
			{
				GC.Collect();
				Resources.UnloadUnusedAssets ();
				Time.timeScale = 1.0f;
				Application.targetFrameRate = 60;
			} 
			else
			{
				GC.Collect();
				Resources.UnloadUnusedAssets ();
				Time.timeScale = 0.0f;
			}
		}  

		/// <summary>
		/// Save the PlayerPrefs when the game is quiting
		/// </summary>
		void OnApplicationQuit()
		{
			Resources.UnloadUnusedAssets ();
			PlayerPrefs.Save();
			MyAdMob.Instance.DestroyBanner ();
			MyAdMob.Instance.RequestBanner ();
		}

		void starmovefinish (){
			StarmoveX5.transform.position = StarmoveX5.transform.parent.position;
			StarmoveX5.gameObject.SetActive (false);
			StarmoveX5.transform.DOKill (true);
			StarmoveX3.transform.position = StarmoveX3.transform.parent.position;
			StarmoveX3.gameObject.SetActive (false);
			StarmoveX3.transform.DOKill (true);
			StarmoveOntime.transform.position = StarmoveOntime.transform.parent.position;
			StarmoveOntime.gameObject.SetActive (false);
			StarmoveOntime.transform.DOKill (true);
		}
		/// <summary>
		/// Animation when the player lose
		/// </summary>
		public void AnimationCameraGameOver()
		{
			starmovefinish ();
			if(isGameOver)
				return;

			#if VS_SHARE
			VSSHARE.DOTakeScreenShot();
			#endif

			DOTween.KillAll ();
			if (firsttime) {
				firsttime = false;
			} else {
				soundManager.PlaySoundFail ();
			}
			isGameOver = true;

			StopAllCoroutines ();


			playerCanShoot = false;

			if(OnFailStart != null)
				OnFailStart();

		//	Color colorFrom = constant.BackgroundColor;
			Color colorFrom = Camera.main.backgroundColor;
			Color colorTo = constant.FailColor;

			float delay = 0.7f;


	//		Camera.main.backgroundColor = colorFrom;

			Camera.main.DOColor (colorTo, delay).OnComplete(() => {

				DOVirtual.DelayedCall(delay, () => {
					if(OnFailComplete != null)
						OnFailComplete();
				});
				Camera.main.DOColor (colorFrom, delay).SetDelay(delay).OnComplete (() => {

					Camera.main.DOColor (colorFrom, delay).SetDelay(delay/2).OnComplete(() =>{
					});

				});


			});



			Camera.main.transform.DOShakePosition (delay, 1, 10, 90, false);

		}


		/// <summary>
		/// If using Very Simple Leaderboard by App Advisory, report the score : http://u3d.as/qxf
		/// </summary>
		void ReportScoreToLeaderboard(int p)
		{
			if (Social.localUser.authenticated) {
				Social.ReportScore (p, Constant.leaderboard, (bool success) =>
					{
						if (success) {
							Debug.Log ("Update Score Success");

						} else {
							Debug.Log ("Update Score Fail");
						}
					});
			}

			#if APPADVISORY_LEADERBOARD
			LeaderboardManager.ReportScore(p);
			#else
	//		print("Get very simple leaderboard to use it : http://u3d.as/qxf");
			#endif
		}
			
		/// <summary>
		/// Animation when the player cleared the level
		/// </summary>
		public void AnimationCameraSuccess()
		{
			if(isGameOver)
				return;

			#if VS_SHARE
			VSSHARE.DOTakeScreenShot();
			#endif

		//	ShowAds();

			ReportScoreToLeaderboard(this.LEVEL.levelNumber);
			ReportScoreToAchievement ();
			playerCanShoot = false;
			soundManager.PlaySoundSuccess ();
			isGameOver = true;
			Checkstars ();
			if(OnSuccessStart != null)
				OnSuccessStart();

			if (OnUpdatelist != null) {
				OnUpdatelist (this.LEVEL.levelNumber);
			}


		//	Color colorFrom = constant.BackgroundColor;
			Color colorFrom = Camera.main.backgroundColor;
			Color colorTo = constant.SuccessColor;


			float delay = 0.10f;

	//		Camera.main.backgroundColor = colorFrom;
	
			Camera.main.DOColor (colorTo, delay);
			CircleBorder.DOScale (Vector3.one * 2f, delay * 4).OnComplete (() => {
				CircleBorder.DOScale (Vector3.one, delay*4).OnComplete (() => {
					Camera.main.DOColor (colorFrom, delay);
				});

	//			if(OnSuccessComplete != null && (PlayerPrefs.GetInt ("LevelNotPass", 1) == 1) )
	//				OnSuccessComplete();
			});
			StartCoroutine(CreateGameAfter(3f));
		}

		IEnumerator CreateGameAfter(float seconds)
		{
			yield return new WaitForSeconds(seconds);
			if(OnSuccessComplete != null )
				OnSuccessComplete();
			print("Done " + Time.time);
		}


		void Runtext (string textrun){
			TextAchievement.text = textrun;
			TextAchievement.transform.DOScale (Vector3.one * 2f, 1f).OnComplete (() => {
				TextAchievement.transform.DOScale (Vector3.one * 0f, 0.5f);
			});
		}

		IEnumerator fairy_Dust(){
			FairyDust.Play ();
			yield return new WaitForSeconds(2f);
			FairyDust.Stop();
		}
		///
		/// save stars
		///
		public void ReportScoreToAchievement (){
			print ("ReportScoreToAchievement1" + PlayerPrefs.GetInt ("STARS", 0));
			if (Social.localUser.authenticated) {
				if (this.LEVEL.levelNumber <=100){
					PlayGamesPlatform.Instance.IncrementAchievement(Constant.AchieveIncre_sakura, Constant.Achievestep, success => {});
				} else if (this.LEVEL.levelNumber >100 && this.LEVEL.levelNumber <=200){
					PlayGamesPlatform.Instance.IncrementAchievement(Constant.AchieveIncre_Iris, Constant.Achievestep, success => {});
				}else if (this.LEVEL.levelNumber >200 && this.LEVEL.levelNumber <=250){
					PlayGamesPlatform.Instance.IncrementAchievement(Constant.AchieveIncre_hisbicus, Constant.Achievestep, success => {});
				}else if (this.LEVEL.levelNumber >250 && this.LEVEL.levelNumber <=300){
					PlayGamesPlatform.Instance.IncrementAchievement(Constant.AchieveIncre_columbine, Constant.Achievestep, success => {});
				}else if (this.LEVEL.levelNumber >300 && this.LEVEL.levelNumber <=400){
					PlayGamesPlatform.Instance.IncrementAchievement(Constant.AchieveIncre_plumeria, Constant.Achievestep, success => {});
				}else if (this.LEVEL.levelNumber >400 && this.LEVEL.levelNumber <=500){
					PlayGamesPlatform.Instance.IncrementAchievement(Constant.AchieveIncre_lotus, Constant.Achievestep, success => {});
				}else if (this.LEVEL.levelNumber >500 && this.LEVEL.levelNumber <=550){
					PlayGamesPlatform.Instance.IncrementAchievement(Constant.AchieveIncre_red_rose, Constant.Achievestep, success => {});
				}else if (this.LEVEL.levelNumber >550 ){
					PlayGamesPlatform.Instance.IncrementAchievement(Constant.AchieveIncre_yellow_rose, Constant.Achievestep, success => {});
				}

			}

			if (constant.AchieComboX3 && (PlayerPrefs.GetInt ("AchiedComboX3", 0) ==0) ) {
				if (_comboX3) {
					constant.AchieComboX3 = false;
					PlayerPrefs.SetInt ("STARS", PlayerPrefs.GetInt ("STARS", 0) + 5);
					PlayerPrefs.SetInt ("AchiedComboX3", 1);
					Social.ReportProgress (Constant.Achieve_beginner, 100, success => {});
					int temp = Int32.Parse (textStars.text) + 5;
					StartCoroutine ("fairy_Dust");
					textStars.text = temp.ToString();
					Runtext ("Achievement:Bonus 5 stars");

			}
			}

			if (constant.AchieComboX5 && (PlayerPrefs.GetInt ("AchiedComboX5", 0) ==0)) {
				if (_comboX5) {
					constant.AchieComboX5 = false;
					PlayerPrefs.SetInt ("STARS", PlayerPrefs.GetInt ("STARS", 0) + 10);
					PlayerPrefs.SetInt ("AchiedComboX5", 1);
					Social.ReportProgress (Constant.Achieve_immediater, 100, success => {});
					int temp = Int32.Parse (textStars.text) + 10;
					StartCoroutine ("fairy_Dust");
					textStars.text = temp.ToString();
					Runtext ("Achievement:Bonus 10 stars");
				}
			}
			if (this.LEVEL.levelNumber >= 50 && (constant.Achiefinish5s) && (PlayerPrefs.GetInt ("Achiefinish5s", 0) ==0)) {
				timeAchie = timeAchie - timeleft;
				if (timeAchie < 5){
					constant.Achiefinish5s = false;
					PlayerPrefs.SetInt ("STARS", PlayerPrefs.GetInt ("STARS", 0) + 15);
					PlayerPrefs.SetInt ("Achiefinish5s", 1);
					Social.ReportProgress (Constant.Achieve_professional, 100, success => {});
					int temp = Int32.Parse (textStars.text) + 15;
					StartCoroutine ("fairy_Dust");
					textStars.text = temp.ToString();
					Runtext ("Achievement:Bonus 15 stars");
				}
			}

			if (this.LEVEL.levelNumber >= 150 && (constant.Achiefinish7s) && (PlayerPrefs.GetInt ("Achiefinish7s", 0) ==0)) {
				timeAchie = timeAchie - timeleft;
				if (timeAchie < 7){
					constant.Achiefinish7s = false;
					PlayerPrefs.SetInt ("STARS", PlayerPrefs.GetInt ("STARS", 0) + 20);
					PlayerPrefs.SetInt ("Achiefinish7s", 1);
					Social.ReportProgress (Constant.Achieve_master, 100, success => {});
					int temp = Int32.Parse (textStars.text) + 20;
					StartCoroutine ("fairy_Dust");
					textStars.text = temp.ToString();
					Runtext ("Achievement:Bonus 20 stars");
				}
			}

			if (this.LEVEL.levelNumber >= 250 && (constant.Achiefinish10s)&& (PlayerPrefs.GetInt ("Achiefinish10s", 0) ==0)) {
				timeAchie = timeAchie - timeleft;
				if (timeAchie < 10){
					constant.Achiefinish10s = false;
					PlayerPrefs.SetInt ("STARS", PlayerPrefs.GetInt ("STARS", 0) + 25);
					PlayerPrefs.SetInt ("Achiefinish10s", 1);
					Social.ReportProgress (Constant.Achieve_legend, 100, success => {});
					int temp = Int32.Parse (textStars.text) + 25;
					StartCoroutine ("fairy_Dust");
					textStars.text = temp.ToString();
					Runtext ("Achievement:Bonus 25 stars");
				}
			}
			print ("ReportScoreToAchievement2" + PlayerPrefs.GetInt ("STARS", 0));
		}

		///
		///
		///
		public void Checkstars()
		{
			print ("Checkstars1" + PlayerPrefs.GetInt ("STARS", 0));
			int count_stars = PlayerPrefs.GetInt ("STARS", 0);

			if (PlayerPrefs.GetInt ("Level" + this.LEVEL.levelNumber + "Ontime") == 0){
				if (timeleft >0){
					PlayerPrefs.SetInt ("Level" + this.LEVEL.levelNumber + "Ontime", 1 );
					count_stars++;
					DomoveStar(StarmoveOntime, 3f);
				}
			}
			if (PlayerPrefs.GetInt ("Level" + this.LEVEL.levelNumber + "ComboX3") == 0){
				if (_comboX3){
					PlayerPrefs.SetInt ("Level" + this.LEVEL.levelNumber + "ComboX3", 1 );
					count_stars++;
				}
			}
			if (PlayerPrefs.GetInt ("Level" + this.LEVEL.levelNumber + "ComboX5") == 0){
				if (_comboX5){
					PlayerPrefs.SetInt ("Level" + this.LEVEL.levelNumber + "ComboX5", 1 );
					count_stars++;
				}
			}

			PlayerPrefs.SetInt ("STARS", count_stars);
		//	PlayerPrefs.SetInt ("STARS", 286);
			if (this.LEVEL.levelNumber == 100 && maxLevel <= 100) {
				if (count_stars <= 285) {
					PlayerPrefs.SetInt ("LevelNotPass", 0);
				} else {
					PlayerPrefs.SetInt ("LevelNotPass", 1 );
				}
				print ("LevelNotPass" + PlayerPrefs.GetInt ("LevelNotPass", 0));
			} else if (this.LEVEL.levelNumber == 200 && maxLevel <= 200) {
				if (count_stars <= 570) {
					PlayerPrefs.SetInt ("LevelNotPass", 0);
				}else {
					PlayerPrefs.SetInt ("LevelNotPass", 1 );
				};

			} else if (this.LEVEL.levelNumber == 250 && maxLevel <= 250) {
				if (count_stars <= 715) {
					PlayerPrefs.SetInt ("LevelNotPass", 0);
				}else {
					PlayerPrefs.SetInt ("LevelNotPass", 1 );
				}
			} else if (this.LEVEL.levelNumber == 300 && maxLevel <= 300) {
				if (count_stars <= 840) {
					PlayerPrefs.SetInt ("LevelNotPass", 0);
				} else {
					PlayerPrefs.SetInt ("LevelNotPass", 1 );
				}
			} else if (this.LEVEL.levelNumber == 400 && maxLevel <= 400) {
				if (count_stars <= 1100) {
					PlayerPrefs.SetInt ("LevelNotPass", 0);
				} else {
					PlayerPrefs.SetInt ("LevelNotPass", 1 );
				}
			} else if (this.LEVEL.levelNumber == 500 && maxLevel <= 500) {
				if (count_stars <= 1340) {
					PlayerPrefs.SetInt ("LevelNotPass", 0);
				} else {
					PlayerPrefs.SetInt ("LevelNotPass", 1 );
				}
			} else if (this.LEVEL.levelNumber == 550 && maxLevel <= 550) {
				if (count_stars <= 1440) {
					PlayerPrefs.SetInt ("LevelNotPass", 0);
				} else {
					PlayerPrefs.SetInt ("LevelNotPass", 1 );
				}
			} else {
				PlayerPrefs.SetInt ("LevelNotPass", 1 );
			}
			print ("Checkstars2" + PlayerPrefs.GetInt ("STARS", 0));

		}
		/// <summary>
		/// If using Very Simple Ads by App Advisory, show an interstitial if number of play > numberOfPlayToShowInterstitial: http://u3d.as/oWD
		/// </summary>
		/*
		public void ShowAds()
		{
			int count = PlayerPrefs.GetInt("GAMEOVER_COUNT",0);
			count++;
			PlayerPrefs.SetInt("GAMEOVER_COUNT",count);
			PlayerPrefs.Save();
			if (count % 3 ==0)
			{
				Debug.LogWarning("Very Simple Ad is already implemented in this asset" + count);
		//		MyAdMob.Instance.ShowInterstitial ();
			}
			//MyAdMob.Instance.ShowInterstitial ();
				
#if APPADVISORY_ADS
			if(count > numberOfPlayToShowInterstitial)
			{
			#if UNITY_EDITOR
			print("count = " + count + " > numberOfPlayToShowINterstitial = " + numberOfPlayToShowInterstitial);
			#endif
			if(AdsManager.instance.IsReadyInterstitial())
			{
			#if UNITY_EDITOR
				print("AdsManager.instance.IsReadyInterstitial() == true ----> SO ====> set count = 0 AND show interstial");
			#endif
				PlayerPrefs.SetInt("GAMEOVER_COUNT",0);
				AdsManager.instance.ShowInterstitial();
			}
			else
			{
			#if UNITY_EDITOR
				print("AdsManager.instance.IsReadyInterstitial() == false");
			#endif
			}

		}
		else
		{
			PlayerPrefs.SetInt("GAMEOVER_COUNT", count);
		}
		PlayerPrefs.Save();
			#else
		if(count >= numberOfPlayToShowInterstitial)
		{
			Debug.LogWarning("To show ads, please have a look to Very Simple Ad on the Asset Store, or go to this link: " + VerySimpleAdsURL);
			Debug.LogWarning("Very Simple Ad is already implemented in this asset");
			Debug.LogWarning("Just import the package and you are ready to use it and monetize your game!");
			Debug.LogWarning("Very Simple Ad : " + VerySimpleAdsURL);
			PlayerPrefs.SetInt("GAMEOVER_COUNT",0);
		}
		else
		{
			PlayerPrefs.SetInt("GAMEOVER_COUNT", count);
		}
		PlayerPrefs.Save();
			#endif
	}
	*/
}
}


#endif