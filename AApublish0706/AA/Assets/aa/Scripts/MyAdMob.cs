﻿using System;
using UnityEngine;
#if UNITY_EDITOR || UNITY_ANDROID || UNITY_IPHONE || UNITY_IOS
using GoogleMobileAds.Api;
using GoogleMobileAds;
#endif

#if AADOTWEEN


namespace AppAdvisory.AA
{

public class MyAdMob : MonoBehaviour
{
    public static MyAdMob Instance;
    
		private int widthscreen
		{
			get
			{
				var height = 2f * Camera.main.orthographicSize;
				var width = height * Camera.main.aspect;
				return (int)width;
			}
		}

#if UNITY_EDITOR || UNITY_ANDROID || UNITY_IPHONE || UNITY_IOS    
    private string AndroidBannerUnitId = "ca-app-pub-7809921132778439/1774430300";
	private string AndroidInterstitialUnitId = "ca-app-pub-7809921132778439/4727896708";
//    private string IosBannerUnitId = "ca-app-pub-7361288100078402/3598153774";
//    private string IosInterstitialUnitId = "ca-app-pub-7361288100078402/8028353376";
    private BannerView bannerView;
    private InterstitialAd interstitial;
#endif

#if UNITY_WP8 || UNITY_WP_8_1
    public event Action WPRequestBanner;
    public event Action WPShowBanner;
    public event Action WPHideBanner;
    public event Action WPRequestInterstitial;
    public event Action WPShowInterstitial;
#endif

    void Awake()
    {
        if (Instance == null) Instance = this;
	
    }

    void Start()
    {
        RequestInterstitial();
        RequestBanner();
    }

    public void RequestBanner()
    {        
#if UNITY_EDITOR
        string adUnitId = AndroidBannerUnitId;        
#elif UNITY_ANDROID
            string adUnitId = AndroidBannerUnitId;
#elif UNITY_IPHONE || UNITY_IOS
            string adUnitId = IosBannerUnitId;        
#endif

#if UNITY_EDITOR || UNITY_ANDROID || UNITY_IPHONE || UNITY_IOS
        // Create a 320x50 banner at the top of the screen.
        // bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
        // Create custome banner
    //    AdSize AdSizecustom = new AdSize (widthscreen, 50); 
        bannerView = new BannerView(adUnitId, AdSize.SmartBanner , AdPosition.Bottom);
        // Register for ad events.
bannerView.OnAdLoaded += HandleAdLoaded;
        bannerView.OnAdFailedToLoad += HandleAdFailedToLoad;
bannerView.OnAdOpening += HandleAdOpened;
bannerView.OnAdClosed += HandleAdClosing;
bannerView.OnAdLeavingApplication += HandleAdLeftApplication;
        // Load a banner ad.
        bannerView.LoadAd(createAdRequest());
#endif
        
#if UNITY_WP8 || UNITY_WP_8_1
        if (WPRequestBanner != null)
        {
            WPRequestBanner();
        }
#endif
    }

    public void ShowBanner()
    {
#if UNITY_EDITOR || UNITY_ANDROID || UNITY_IPHONE || UNITY_IOS
        if (bannerView != null)
        {
            bannerView.Show();
        }
#endif

#if UNITY_WP8 || UNITY_WP_8_1
        if (WPShowBanner != null)
        {
            WPShowBanner();
        }
#endif
    }

    public void HideBanner()
    {
#if UNITY_EDITOR || UNITY_ANDROID || UNITY_IPHONE || UNITY_IOS
        if (bannerView != null)
        {
            bannerView.Hide();
        }
#endif


#if UNITY_WP8 || UNITY_WP_8_1
        if (WPHideBanner != null)
        {
            WPHideBanner();
        }
#endif
    }


		public void DestroyBanner()
		{
#if UNITY_EDITOR || UNITY_ANDROID || UNITY_IPHONE || UNITY_IOS
			if (bannerView != null)
			{
				bannerView.Destroy();
			}
#endif
		}

    public void RequestInterstitial()
    {        
#if UNITY_EDITOR
        string adUnitId = AndroidInterstitialUnitId;
#elif UNITY_ANDROID
            string adUnitId = AndroidInterstitialUnitId;
#elif UNITY_IPHONE || UNITY_IOS
            string adUnitId = IosInterstitialUnitId;
#endif

#if UNITY_EDITOR || UNITY_ANDROID || UNITY_IPHONE || UNITY_IOS
        // Create an interstitial.
        interstitial = new InterstitialAd(adUnitId);
        // Register for ad events.
interstitial.OnAdLoaded += HandleInterstitialLoaded;
interstitial.OnAdFailedToLoad += HandleInterstitialFailedToLoad;
interstitial.OnAdOpening += HandleInterstitialOpened;
interstitial.OnAdClosed += HandleInterstitialClosing;
interstitial.OnAdLeavingApplication += HandleInterstitialLeftApplication;
        // Load an interstitial ad.
        interstitial.LoadAd(createAdRequest());
#endif

#if UNITY_WP8 || UNITY_WP_8_1
        if (WPRequestInterstitial != null)
        {
            WPRequestInterstitial();
        }
#endif
    }

#if UNITY_EDITOR || UNITY_ANDROID || UNITY_IPHONE || UNITY_IOS
    // Returns an ad request with custom ad targeting.
    private AdRequest createAdRequest()
    {
        return new AdRequest.Builder()
                .AddTestDevice(AdRequest.TestDeviceSimulator)
                .AddTestDevice("52DDC60A1F2F31A2F9CEB1F7E0B7E1F5")
                .AddTestDevice("F14384479A27BD51DE3F190D26DDD325")
                .AddTestDevice("234b014a1088328e45457477b01b8948")
                .AddKeyword("game")
                .SetGender(Gender.Male)
                .TagForChildDirectedTreatment(false)
                .Build();

    }

#endif

#if UNITY_EDITOR || UNITY_ANDROID || UNITY_IPHONE || UNITY_IOS
		// Returns an ad request with custom ad targeting.
		public void Destroy()
		{
			interstitial.Destroy();

		}

#endif
		
    public void ShowInterstitial()
    {
#if UNITY_EDITOR || UNITY_ANDROID || UNITY_IPHONE || UNITY_IOS
        if (interstitial.IsLoaded())
        {
            interstitial.Show();
	
        }
        else
        {
            Debug.Log("Interstitial is not ready yet.");
        }
#endif

#if UNITY_WP8 || UNITY_WP_8_1
        if (WPShowInterstitial != null)
        {
            WPShowInterstitial();
        }
#endif
    }

    #region callback handlers
#if UNITY_EDITOR || UNITY_ANDROID || UNITY_IPHONE || UNITY_IOS
    public void HandleAdLoaded(object sender, EventArgs args)
    {
        Debug.Log("HandleAdLoaded event received.");
    }

    public void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        Debug.Log("HandleFailedToReceiveAd event received with message: " + args.Message);
    }

    public void HandleAdOpened(object sender, EventArgs args)
    {
        Debug.Log("HandleAdOpened event received");
    }

    void HandleAdClosing(object sender, EventArgs args)
    {
        Debug.Log("HandleAdClosing event received");
    }

    public void HandleAdClosed(object sender, EventArgs args)
    {
        Debug.Log("HandleAdClosed event received");
    }

    public void HandleAdLeftApplication(object sender, EventArgs args)
    {
        Debug.Log("HandleAdLeftApplication event received");
    }

    public void HandleInterstitialLoaded(object sender, EventArgs args)
    {
        Debug.Log("HandleInterstitialLoaded event received.");
    }

    public void HandleInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        Debug.Log("HandleInterstitialFailedToLoad event received with message: " + args.Message);
    }

    public void HandleInterstitialOpened(object sender, EventArgs args)
    {
        Debug.Log("HandleInterstitialOpened event received");
    }

    void HandleInterstitialClosing(object sender, EventArgs args)
    {
        Debug.Log("HandleInterstitialClosing event received");
    }

    public void HandleInterstitialClosed(object sender, EventArgs args)
    {
        Debug.Log("HandleInterstitialClosed event received");
    }

    public void HandleInterstitialLeftApplication(object sender, EventArgs args)
    {
        Debug.Log("HandleInterstitialLeftApplication event received");
    }
#endif
    #endregion
}


}

#endif