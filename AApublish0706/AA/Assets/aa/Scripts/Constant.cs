﻿
/***********************************************************************************************************
 * Produced by App Advisory	- http://app-advisory.com													   *
 * Facebook: https://facebook.com/appadvisory															   *
 * Contact us: https://appadvisory.zendesk.com/hc/en-us/requests/new									   *
 * App Advisory Unity Asset Store catalog: http://u3d.as/9cs											   *
 * Developed by Gilbert Anthony Barouch - https://www.linkedin.com/in/ganbarouch                           *
 ***********************************************************************************************************/

using UnityEngine;
using System.Collections;
using System;


#if AADOTWEEN


namespace AppAdvisory.AA
{
	/// <summary>
	/// Class where is define some constants use in the game
	/// </summary>
	public class Constant : MonobehaviourHelper
	{
		/// <summary>
		/// Each time the tweener complete a sequence, we anim - or not - the line who link the dot to the center. If you don't want this animation, turn this boolean to false
		/// </summary>
		public bool AnimLineAtEachTurn = true;
		public bool AchieComboX3 = true;
		public bool AchieComboX5 = true;
		public bool Achiefinish5s = true;
		public bool Achiefinish7s = true;
		public bool Achiefinish10s = true;
		/// <summary>
		/// Size fo the line who link each dots to the center circle
		/// </summary>
		public const int LINE_WIDHT = 6;
		public const int Achievestep = 1;

		public const string LAST_LEVEL_PLAYED = "LEVEL_PLAYED";
		public const string LEVEL_UNLOCKED = "LEVEL";
		public const string SOUND_ON = "SOUND_ON";
		public const string SOUNDFSX_ON = "FSX_ON";
		public const string SOUNDBGM_ON = "BGM_ON";
		public const string leaderboard = "CgkIqJ_7pf8NEAIQAQ";
		public const string Achieve_beginner = "CgkIqJ_7pf8NEAIQAg";
		public const string Achieve_immediater = "CgkIqJ_7pf8NEAIQAw";
		public const string Achieve_professional = "CgkIqJ_7pf8NEAIQBA";
		public const string Achieve_master = "CgkIqJ_7pf8NEAIQBQ";
		public const string Achieve_legend = "CgkIqJ_7pf8NEAIQBg";
		public const string AchieveIncre_sakura = "CgkIqJ_7pf8NEAIQBw";
		public const string AchieveIncre_Iris = "CgkIqJ_7pf8NEAIQCA";
		public const string AchieveIncre_hisbicus = "CgkIqJ_7pf8NEAIQCQ";
		public const string AchieveIncre_columbine = "CgkIqJ_7pf8NEAIQCg";
		public const string AchieveIncre_plumeria = "CgkIqJ_7pf8NEAIQCw";
		public const string AchieveIncre_lotus = "CgkIqJ_7pf8NEAIQDA";
		public const string AchieveIncre_red_rose = "CgkIqJ_7pf8NEAIQDQ";
		public const string AchieveIncre_yellow_rose = "CgkIqJ_7pf8NEAIQDg";
		/// <summary>
		/// Background color when the player lose
		/// </summary>
		public Color FailColor;

		/// <summary>
		/// Background color when the player win
		/// </summary>
		public Color SuccessColor;

		/// <summary>
		/// Default background color
		/// </summary>
		public Color BackgroundColor;
		public Color Background1;
		public Color Background2;
		public Color Background3;
		/// <summary>
		/// Dot color
		/// </summary>
		public Color DotColor;

		/// <summary>
		/// ChangeColor
		/// </summary>
		public Color ChangeColor;
	}
}


#endif