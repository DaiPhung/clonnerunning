
/***********************************************************************************************************
 * Produced by App Advisory	- http://app-advisory.com													   *
 * Facebook: https://facebook.com/appadvisory															   *
 * Contact us: https://appadvisory.zendesk.com/hc/en-us/requests/new									   *
 * App Advisory Unity Asset Store catalog: http://u3d.as/9cs											   *
 * Developed by Gilbert Anthony Barouch - https://www.linkedin.com/in/ganbarouch                           *
 ***********************************************************************************************************/

using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;


#if AADOTWEEN


namespace AppAdvisory.AA
{
	/// <summary>
	/// Class in charge to prompt a popup to ask the player to rate the game
	///
	/// Attached to the "RateUsManager" GameObject
	/// </summary>
	public class Cheer_prompt : MonoBehaviour 
	{
		/// <summary>
		/// The number of level played (each win or lose count for 1 play) to prompt the popup
		/// </summary>
		public GameObject image1;
		public GameObject image2;
		public GameObject image3;
		public GameObject image_bg1;
		public GameObject image2_bg2;
		public GameObject image3_bg3;
	//	public Button btnNever;
		public Button btnNext;

		public GameObject btnBack;
		public GameObject LevelTextGO;
		public GameObject IntroSceneGo;

		public CanvasGroup popupCanvasGroup;

		void Awake()
		{
			popupCanvasGroup.alpha = 0;
			popupCanvasGroup.gameObject.SetActive(false);
		}

		void OnEnable()
		{
			GameManager.OnSuccessComplete += PromptPopup;
		}

		void OnDisable()
		{
			GameManager.OnSuccessComplete -= PromptPopup;

		}
		public Text m_CStarsPopText;
		int lastLevel
		{
			get 
			{
				return PlayerPrefs.GetInt (Constant.LAST_LEVEL_PLAYED, 1);
			}
		}
		int maxLevel
		{
			get 
			{
				return PlayerPrefs.GetInt (Constant.LEVEL_UNLOCKED, 1);
			}
		}
		int countstart
		{
			get 
			{
				return PlayerPrefs.GetInt ("STARS", 0);
			}
		}
		void AddButtonListeners()
		{
			btnNext.onClick.AddListener(OnClickedYes);
		}

		void RemoveButtonListener()
		{
			btnNext.onClick.RemoveListener(OnClickedYes);
		}

		void OnClickedYes()
		{
			HidePopup();
			Checkstars();
			CheckAdmob ();
			btnBack.SetActive (true);
		}

		void Checkstars(){
			print ("Checkstars_LevelNotPass" + PlayerPrefs.GetInt ("LevelNotPass", 0));
			print ("lastLevel" + lastLevel);
			if (PlayerPrefs.GetInt ("LevelNotPass", 1) == 0) {
				if (countstart <= 285 && (lastLevel-1) <=100) {
					m_CStarsPopText.text = "Oops! You need 285 more Stars to unlock the next level";
				} else if (countstart <= 570 && ( lastLevel > 100 && lastLevel <=200)) {
					PlayerPrefs.SetInt ("LevelNotPass", 0);
					m_CStarsPopText.text = "Oops! You need 570 more Stars to unlock the next level";
				} else if (countstart <= 715 && lastLevel >200 && lastLevel <=250) {
					PlayerPrefs.SetInt ("LevelNotPass", 0);
					m_CStarsPopText.text = "Oops! You need 715 more Stars to unlock the next level";
				} else if (countstart <= 840 && lastLevel >250 && lastLevel <=300) {
					PlayerPrefs.SetInt ("LevelNotPass", 0);
					m_CStarsPopText.text = "Oops! You need 840 more Stars to unlock the next level";
				} else if (countstart <= 1100 && lastLevel >300 && lastLevel <=400) {
					PlayerPrefs.SetInt ("LevelNotPass", 0);
					m_CStarsPopText.text = "Oops! You need 1100 more Stars to unlock the next level";
				} else if (countstart <= 1340 && lastLevel >400 && lastLevel <=500) {
					PlayerPrefs.SetInt ("LevelNotPass", 0);
					m_CStarsPopText.text = "Oops! You need 1340 more Stars to unlock the next level";
				} else if (countstart <= 1440 && lastLevel >500 && lastLevel <=550) {
					PlayerPrefs.SetInt ("LevelNotPass", 0);
					m_CStarsPopText.text = "Oops! You need 1440 more Stars to unlock the next level";
				}
				FindObjectOfType<PromptStars> ().PromptPopup ();

			}
		}

		/// <summary>
		/// Method to prompt the popup
		/// </summary>
		public void PromptPopup()
		{
			popupCanvasGroup.alpha = 0;
			LevelTextGO.SetActive (false);
			IntroSceneGo.SetActive (false);
			btnBack.SetActive (false);
			popupCanvasGroup.gameObject.SetActive(true);
			image_bg1.SetActive (false);
			image2_bg2.SetActive (false);
			image3_bg3.SetActive (false);
			StartCoroutine(DoLerpAlpha(popupCanvasGroup, 0, 1, 0.3f, () => {
				FindObjectOfType<InputTouch>().BLOCK_INPUT = true;
				AddButtonListeners();
			}));
			if (PlayerPrefs.GetInt ("Level" + lastLevel + "ComboX3") == 1) {
				StartCoroutine(WaitAmountTime(image_bg1));
			} 
			if (PlayerPrefs.GetInt ("Level" + lastLevel + "ComboX5") == 1) {
				StartCoroutine(WaitAmountTime(image2_bg2));
			} 
			if (PlayerPrefs.GetInt ("Level" + lastLevel + "Ontime") == 1){
				StartCoroutine(WaitAmountTime(image3_bg3));
			}
		}

		IEnumerator WaitAmountTime(GameObject imagee)
		{
			yield return new WaitForSeconds(0.7f);
			imagee.SetActive (true);
			yield return new WaitForSeconds(0.5f);
			print ("wait for StartCoroutine visible");
		}

		void CheckAdmob(){
			if (lastLevel % 10 == 0) {
				MyAdMob.Instance.ShowInterstitial ();
			}
			MyAdMob.Instance.Destroy ();
			MyAdMob.Instance.RequestInterstitial ();
		}
		/// <summary>
		/// Method to hide the popup
		/// </summary>
		void HidePopup()
		{
			StartCoroutine(DoLerpAlpha(popupCanvasGroup, 1, 0, 1, () => {

				popupCanvasGroup.gameObject.SetActive(false);
				RemoveButtonListener();
				FindObjectOfType<InputTouch>().BLOCK_INPUT = false;
				LevelTextGO.SetActive (true);
			}));
		}

		public IEnumerator DoLerpAlpha(CanvasGroup c, float from, float to, float time, Action callback)
		{
			float timer = 0;

			c.alpha = from;

			while (timer <= time)
			{
				timer += Time.deltaTime;
				c.alpha = Mathf.Lerp(from, to, timer / time);
				yield return null;
			}

			c.alpha = to;

			if (callback != null)
				callback ();
		}
	}
}

#endif