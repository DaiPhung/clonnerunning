﻿
/***********************************************************************************************************
 * Produced by App Advisory	- http://app-advisory.com													   *
 * Facebook: https://facebook.com/appadvisory															   *
 * Contact us: https://appadvisory.zendesk.com/hc/en-us/requests/new									   *
 * App Advisory Unity Asset Store catalog: http://u3d.as/9cs											   *
 * Developed by Gilbert Anthony Barouch - https://www.linkedin.com/in/ganbarouch                           *
 ***********************************************************************************************************/

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
#if AADOTWEEN
using DG.Tweening;
using System;
using Facebook.Unity;
using Facebook.MiniJSON;


#if APPADVISORY_LEADERBOARD
using AppAdvisory.social;
#endif
#if APPADVISORY_ADS
using AppAdvisory.Ads;
#endif

namespace AppAdvisory.AA
{
	/// <summary>
	/// In Charge to display and managed all the UI elements in the game
	/// </summary>
	public class CanvasManager : MonobehaviourHelper
	{
		/// <summary>
		/// Delegate subscribe by the GameManager and triggered when creating a new level (if player win, or if switching manually the levels)
		/// </summary>
		public delegate void CreateGame(int level);
		public static event CreateGame OnCreateGame;




		/// <summary>
		/// Facebook url open by the native app on mobile
		/// </summary>
		public string facebookApp = "fb://profile/515431001924232" ;
		/// <summary>
		/// Facebook url open by the web browser if failed to open the native app
		/// </summary>
		public string facebookAddress = "https://www.facebook.com/appadvisory";

		AudioSource _music;
		/// <summary>
		/// Audiosource with the music attached (if you add a music to it)
		/// </summary>
		AudioSource music
		{
			get 
			{
				if (_music == null)
					_music = Camera.main.GetComponentInChildren<AudioSource> ();

				return _music;
			}
		}

		/// <summary>
		/// The level displayed on the top of the game view
		/// </summary>
		public Text m_levelText;
		public Text m_chooseText;
		public Text m_StarsText;
		public Text m_StarsTextingame;
		public Text m_CStarsPopText;

		/// <summary>
		/// Change the text of the level displayed on the top of the game view
		/// </summary>
		void SetLevelText(int level)
		{
			this.m_levelText.text =level.ToString();
		}

		/// <summary>
		/// Reference to the IntroMenu GameObject
		/// </summary>
//		public GameObject IntroMenuGO;
		public GameObject LevelPanelGO;
		public GameObject CircleGO;
		public GameObject IntrosceneGo;
		public GameObject MenuThemGo;
		public GameObject LevelManagerGo;
		private GameObject[] Dots;
		public GameObject FBholder;
		public GameObject ScrollViewFB;
		public Button buttonNextLevel;
		public Button buttonLastLevel;
		public Button buttonIntroscene;
		public Button buttonPlayIntro;
		public Button buttonBackLevel;
		public Button buttonToMenu;
		/// <summary>
		/// Get the max level the player could play. A level is playable if the player unlock the previous level. for exemple: to player the level 10, the player have to cleared the level 
		/// </summary>
		int maxLevel
		{
			get 
			{
				return PlayerPrefs.GetInt (Constant.LEVEL_UNLOCKED, 1);
			}
		}

		/// <summary>
		/// Get the last level the player played
		/// </summary>
		int lastLevel
		{
			get 
			{
				return PlayerPrefs.GetInt (Constant.LAST_LEVEL_PLAYED, 1);
			}
		}

		int countstart
		{
			get 
			{
				return PlayerPrefs.GetInt ("STARS", 0);
			}
		}

		/// <summary>
		/// Reference to the setting button who will display some icons with an animation when the player click/tap on the settign button
		/// </summary>
		GridLayoutGroup gridLayoutGroup;


		void OnEnable()
		{
			GameManager.OnSuccessStart += OnSuccessStart;
			GameManager.OnSuccessComplete += OnSuccessComplete;
			GameManager.OnFailStart += OnFailStart;
			GameManager.OnFailComplete += OnFailComplete;
			LevelManagerNew.OnPlayLevel += PlayLevel;
		}

		void OnDisable()
		{
			GameManager.OnSuccessStart -= OnSuccessStart;
			GameManager.OnSuccessComplete -= OnSuccessComplete;
			GameManager.OnFailStart -= OnFailStart;
			GameManager.OnFailComplete -= OnFailComplete;
			LevelManagerNew.OnPlayLevel -= PlayLevel;
		}

		/// <summary>
		/// Called when GameManager trigger the delegate OnSuccessStart
		/// </summary>
		void OnSuccessStart()
		{
		//	buttonUnlock.transform.DOScale(Vector3.zero,0.3f);	
		}

		/// <summary>
		/// Called when GameManager trigger the delegate OnSuccessComplete. Will create the next level
		/// </summary>
		void OnSuccessComplete()
		{
			print ("playnextlevel");
			if (PlayerPrefs.GetInt ("LevelNotPass", 1) == 1){
			PlayNextLevel ();
			} else {
			ReplayCurrentLevel (lastLevel);
			}
		}

		/// <summary>
		/// show popup congratulation when finish
		/// </summary>
		/// 

		/// <summary>
		/// check the condition stars to pass next screen
		/// </summary>
		/// 

		/// <summary>
		/// Called when GameManager trigger the delegate OnFailStart. Will show the button unlock if a rewarded video is available
		/// </summary>
		void OnFailStart()
		{
		//	ShowButtonUnlock();
		}

		/// <summary>
		/// Called when GameManager trigger the delegate OnFailComplete. Will restart the current level
		/// </summary>
		void OnFailComplete()
		{
			ReplayCurrentLevel (lastLevel);
		}

		/// <summary>
		/// Set all the UI In Game Buttons
		/// </summary>
		void SetButtons()
		{

			buttonNextLevel.onClick.AddListener (() => {
				ButtonLogic ();
				OnClickedButtonNextLevel();
				ButtonLogic ();
			});

			buttonLastLevel.onClick.AddListener (() => {
				ButtonLogic ();
				OnClickedButtonPreviousLevel();
				ButtonLogic ();
			});

			buttonIntroscene.onClick.AddListener (OnClickedOpenIntroscene);

			buttonPlayIntro.onClick.AddListener (() => {
				m_StarsText.transform.parent.gameObject.SetActive (false);
				LevelPanelGO.GetComponent<CanvasGroup> ().alpha = 0;
				LevelPanelGO.GetComponent<CanvasGroup> ().interactable = false;
				buttonBackLevel.gameObject.SetActive(true);
				LoadScreen.AnimationIntroToGame(()=>{
				});
				StartTheGame();
			});

            
			buttonBackLevel.onClick.AddListener (() => {
				BackTheMenuGame();
			});
			buttonToMenu.onClick.AddListener (() => {
				buttonIntroscene.gameObject.SetActive (true);
				LoadScreen.AnimationIntroToGame(()=>{
				});
				BackTheMenuGame();
			});
		}


		/// <summary>
		/// Open the like page. Please define your URL here
		/// </summary>
		void OnClickedLike()
		{
			Debug.Log ("TODO: replace your like links here");

			float startTime;
			startTime = Time.timeSinceLevelLoad;

			//open the facebook app
			Application.OpenURL(facebookApp);

			if (Time.timeSinceLevelLoad - startTime <= 1f)
			{
				//fail. Open safari.
				Application.OpenURL(facebookAddress);
			}
		}

		/// <summary>
		/// If player clics on the leaderbord button, we call this method. Works only on mobile (iOS & Android) if using Very Simple Leaderboard by App Advisory : http://u3d.as/qxf
		/// </summary>
		public void OnClickedOpenIntroscene()
		{
			IntrosceneGo.SetActive (true);
			m_StarsText.transform.parent.gameObject.SetActive (false);
			LevelPanelGO.GetComponent<CanvasGroup> ().alpha = 0;
			LevelPanelGO.GetComponent<CanvasGroup> ().interactable = false;
			buttonIntroscene.gameObject.SetActive (false);
			m_levelText.transform.parent.gameObject.SetActive(false);
			FindObjectOfType<InputTouch>().BLOCK_INPUT = true;
			LoadScreen.AnimationGameToIntro(()=>{
				//LoadScreen.gameObject.SetActive(true);
			});
		}


		/// <summary>
		/// Call the share method. Please define your methods here.
		/// </summary>
		/// 
		void OnClickedShare()
		{
			Debug.Log ("TODO: put your share code here");
		}

		/// <summary>
		/// Call the rate method of the RateManager. If yhe player click on it, we display immediately the pop up of the RateManager by the method PromptPopup
		/// </summary>
		void OnClickedRate()
		{
			// FindObjectOfType<RateUsManager>().PromptPopup();
			Application.OpenURL ("market://details?id=com.sgt.AaDotsColorFlowers");
		}



		void Awake()
		{
			Application.targetFrameRate = 60;
			if (!PlayerPrefs.HasKey (Constant.LAST_LEVEL_PLAYED)) {
				PlayerPrefs.SetInt (Constant.LAST_LEVEL_PLAYED, 1);
			} 

			if (!PlayerPrefs.HasKey (Constant.LEVEL_UNLOCKED)) {
				PlayerPrefs.SetInt (Constant.LEVEL_UNLOCKED, 1);
			} 

			PlayerPrefs.Save ();



			SetButtons ();


			ButtonLogic ();

		}
		void Update(){
			m_StarsTextingame.text = System.Convert.ToString(PlayerPrefs.GetInt ("STARS", 0));

		}

		/// <summary>
		/// Display the next and/or last button (the arrow around the level at the top of the screen)
		/// </summary>
		void ButtonLogic()
		{

			if (lastLevel == 1)
				SetButtonActive(buttonLastLevel,false);
			else
				SetButtonActive(buttonLastLevel,true);

			if(lastLevel >= maxLevel)
				SetButtonActive(buttonNextLevel,false);
			else
				SetButtonActive(buttonNextLevel,true);
		}

		/// <summary>
		/// Activate and enable - or not - buttons
		/// </summary>
		void SetButtonActive(Button b,bool isActive)
		{
			Color c = b.GetComponent<Image> ().color;

			if (isActive) {
				b.GetComponent<Image> ().color = new Color(c.r,c.g,c.b,1f);
				b.interactable = true;
			}  else {
				b.GetComponent<Image> ().color = new Color(c.r,c.g,c.b,0f);
				b.interactable = false;
			}

		}

		/// <summary>
		/// Call StartTheGameCorout 
		/// </summary>
		public void StartTheGame()
		{
			StartCoroutine ("StartTheGameCorout");
		}

		public void BackTheMenuGame()
		{
			StartCoroutine ("StartBackTheMenuGame");
		}
		/// <summary>
		/// Start PlayLevel method at the next frame
		/// </summary>

		IEnumerator StartTheGameCorout()
		{
			if (FB.IsLoggedIn) {
				FBholder.GetComponent<FBScript> ().QueryScores ();
				ScrollViewFB.SetActive (true);
			} else {
				ScrollViewFB.SetActive (false);
			}
			buttonIntroscene.gameObject.SetActive(true);
			m_chooseText.gameObject.SetActive(false);
			MenuThemGo.SetActive (false);
			m_levelText.transform.parent.gameObject.SetActive(true);
			m_StarsTextingame.transform.parent.gameObject.SetActive (true);
			FindObjectOfType<InputTouch>().BLOCK_INPUT = true;
			yield return 0;
			PlayLevel (lastLevel);
		}
       
		IEnumerator StartBackTheMenuGame()
		{
			LevelManagerGo.GetComponent<LevelManagerNew> ().EnableItem ();
			MenuThemGo.SetActive (true);
			LevelPanelGO.GetComponent<CanvasGroup> ().alpha = 1;
			LevelPanelGO.GetComponent<CanvasGroup> ().interactable = true;
			FindObjectOfType<InputTouch>().BLOCK_INPUT = true;
			buttonBackLevel.gameObject.SetActive (true);
			m_levelText.transform.parent.gameObject.SetActive(false);
			m_chooseText.gameObject.SetActive(true);
			m_StarsText.text = System.Convert.ToString(PlayerPrefs.GetInt ("STARS", 0));
			m_StarsText.transform.parent.gameObject.SetActive (true);
			m_StarsTextingame.transform.parent.gameObject.SetActive (false);
			yield return 0;
			Time.timeScale = 1;

		}
		/*
		/// <summary>
		/// When the player failed, we show an unlock button ONLY IF there is a rewarded video available
		/// </summary>
		void ShowButtonUnlock()
		{
			bool isReadyRewardedVideo = false;

			#if APPADVISORY_ADS
			isReadyRewardedVideo = AdsManager.instance.IsReadyRewardedVideo();
			#endif

			if (isReadyRewardedVideo)
			{
				if (buttonUnlock.transform.localScale.x == 1) {
					buttonUnlock.transform.DOScale (Vector3.one * 1.5f, 0.3f).SetLoops (6, LoopType.Yoyo);
				} else {
					buttonUnlock.transform.DOScale (Vector3.one, 0.3f);
				}
			}
		}
		*/
		/// <summary>
		/// Run the level logic on the UI side
		/// </summary>
		private void PlayLevel(int level)
		{
			SetLevelText (level);

			if(level > maxLevel)
			PlayerPrefs.SetInt (Constant.LEVEL_UNLOCKED, level);

			PlayerPrefs.SetInt (Constant.LAST_LEVEL_PLAYED, level);

			PlayerPrefs.Save ();

			ButtonLogic ();

			if(OnCreateGame != null)
				OnCreateGame(level);
		}


		/// <summary>
		/// Method called when the player clicked on the left arrow on the left of the level text on the top of the screen during the game
		/// </summary>
		private void OnClickedButtonPreviousLevel()
		{
			int last = lastLevel;

			last--;

			if (last < 1)
				last = 1;

			Camera.main.transform.DOMove (new Vector3 (-50, Camera.main.transform.position.y, -10), 0.3f).OnComplete (() => {
				Camera.main.transform.position = new Vector3 (50, Camera.main.transform.position.y, -10);
				Camera.main.orthographicSize = 20f;
				SetLevelText (last);

				PlayLevel (last);
				Camera.main.transform.DOMove (new Vector3 (0, Camera.main.transform.position.y, -10), 0.3f).OnComplete (() => {
				});
			});


		}

		/// <summary>
		/// Method called when the player clicked on the right arrow on the roght of the level text on the top of the screen during the game
		/// </summary>
		private void OnClickedButtonNextLevel()
		{
			PlayNextLevel ();

		}

		/// <summary>
		/// Method called when the player failed and so ... we replay the current level
		/// </summary>
		private void ReplayCurrentLevel(int level)
		{
			Camera.main.transform.DOMove (new Vector3 (0, Camera.main.transform.position.y, -10), 0.3f).OnComplete (() => {
				PlayLevel (level);
			});

		}

		/// <summary>
		/// Method called when the player have to play the next level (if the current level is cleared, or if the payer taps/Clicks on the next button or if the player see a rewarded video to unlock the current level
		/// </summary>
		private void PlayNextLevel()
		{
			int last = lastLevel;

			last++;

			Camera.main.transform.DOMove (new Vector3 (50, Camera.main.transform.position.y, -10), 0.3f).OnComplete (() => {

				Camera.main.transform.position = new Vector3 (-50, Camera.main.transform.position.y, -10);

				Camera.main.orthographicSize = 20f;

				SetLevelText (last);

				PlayLevel (last);

				Camera.main.transform.DOMove (new Vector3 (0, Camera.main.transform.position.y, -10), 0.3f).OnComplete (() => {
				});
			});
		}
	}
}


#endif