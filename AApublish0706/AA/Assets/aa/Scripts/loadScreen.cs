﻿using GooglePlayGames;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.BasicApi;

#if AADOTWEEN
using DG.Tweening;
namespace AppAdvisory.AA
{
public class loadScreen : MonoBehaviour {
//	public Button Play;
	public Button Achievement;
	public Button LeaderBoard;
	public Button Rateus;
	public Button Music;
	public Button BtnMenu;

	public float fadeDuration = 0.2f;

		/// <summary>
		/// Time for the  the panel who move horizontally when transitioning between intro and game
		/// </summary>
	public float hidderAnimDuration = 0.5f;
	RectTransform panelHidder;

	RectTransform container;

	CanvasGroup canvasGroup;

	void Awake()
		{
			GetComponent<Canvas> ().overrideSorting = true;
			GetComponent<Canvas> ().pixelPerfect = false;
			GetComponent<Canvas> ().sortingOrder = 20;

			canvasGroup = GetComponent<CanvasGroup> ();

	//		panelHidder = transform.FindChild ("PanelHidder").GetComponent<RectTransform>();

	//		container = transform.FindChild ("Container").GetComponent<RectTransform>();

		}
	// Use this for initialization
	void Start () {
			canvasGroup.alpha = 1;
			PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();

			PlayGamesPlatform.InitializeInstance(config);
			// recommended for debugging:
			PlayGamesPlatform.DebugLogEnabled = true;
			// Activate the Google Play Games platform
			PlayGamesPlatform.Activate();

			Social.localUser.Authenticate((bool success) => {
				if (success) {
					Debug.Log("You've successfully logged in");
				} else {
					Debug.Log("Login failed for some reason");
				}
			});

		//	Button btnPlay = Play.GetComponent<Button> ();
		//	btnPlay.onClick.AddListener (OnClickedPlay);

			Button btnRate = Rateus.GetComponent<Button> ();
			btnRate.onClick.AddListener (OnClickedRate);

			Button btnAchi = Achievement.GetComponent<Button> ();
			btnAchi.onClick.AddListener (OnClickedAchivement);

			Button btnLeaderB = LeaderBoard.GetComponent<Button> ();
			btnLeaderB.onClick.AddListener (OnClickedLeaderBoard);

			Button btnMusic = Music.GetComponent<Button> ();
			btnMusic.onClick.AddListener (OnClickedMusicSetting);
	}

	// Update is called once per frame
	void Update () {
		
	}

		void OnClickedRate()
		{
			//	SceneManager.LoadScene("aa", LoadSceneMode.Single);
			Application.OpenURL("market://details?id=com.sgt.AaDotsColorFlowers");
		}
	void OnClickedAchivement()
	{
			if (Social.localUser.authenticated) {
				((PlayGamesPlatform)Social.Active).ShowAchievementsUI();
			}
	}

		void OnClickedLeaderBoard()
		{
			if (Social.localUser.authenticated) {
				((PlayGamesPlatform)Social.Active).ShowLeaderboardUI (Constant.leaderboard);
			}
		}

		void OnClickedMusicSetting()
		{
			FindObjectOfType<SoundSetting>().PromptPopup();
		}


		public void AnimationIntroToGame(Action callback)
		{
			canvasGroup.alpha = 0;
			canvasGroup.interactable = false;
			canvasGroup.blocksRaycasts = false;
			if(callback != null)
				callback();

		}

		public void AnimationGameToIntro(Action callback)
		{
			canvasGroup.alpha = 1;
			canvasGroup.interactable = true;
			canvasGroup.blocksRaycasts = true;
			if(callback != null)
				callback();
		}


		public void DoFade(CanvasGroup c, float from, float to, float time, Action callback)
		{
			StartCoroutine (DoLerpAlpha (c, from, to, time, callback));
		}


		public IEnumerator DoLerpAlpha(CanvasGroup c, float from, float to, float time, Action callback)
		{
			float timer = 0;

			c.alpha = from;

			while (timer <= time)
			{
				timer += Time.deltaTime;
				c.alpha = Mathf.Lerp(from, to, timer / time);
				yield return null;
			}

			c.alpha = to;

			if (callback != null)
				callback ();
		}


}
}

#endif