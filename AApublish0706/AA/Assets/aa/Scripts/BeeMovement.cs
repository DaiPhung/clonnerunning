﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AppAdvisory.AA
{
public class BeeMovement : MonoBehaviour {

	// Use this for initialization
	public float range_li =25f;
	public float speed = 2f;
	private float _temp = 270f;
	private int level;
	GameManager _gameManager;
	GameManager gameManager
	{
		get
		{
			if(_gameManager == null)
				_gameManager = FindObjectOfType<GameManager>();

			return _gameManager;
		}
	}

	private float positionTouchBorder
	{
		get
		{
			var height = 2f * Camera.main.orthographicSize;
			var width = height * Camera.main.aspect;
			return Mathf.Min(width,height) * 1f / 4f * 1f;
		}
	}

	void Start () {

	}

		void OnEnable()
		{
			GameManager.OnUpdatelevel += Updatelevel;
		}

		/// <summary>
		/// Remove all listeners 
		/// </summary>
		void OnDisable()
		{
			GameManager.OnUpdatelevel -= Updatelevel;
		}
	
	// Update is called once per frame
	void Update () {
		transform.Translate(Vector3.up * speed * Time.deltaTime);
		float position = transform.position.x;
		range_li = positionTouchBorder + 3.5f;
		if (position <= -range_li) {
			TurnBack (_temp);
		} else if (position >= range_li) {
			TurnBack (-_temp);
		}
		}
	void TurnBack( float _temp){
		Quaternion temp = transform.rotation;
		temp.eulerAngles = new Vector3 (0f, 0f, _temp);
		transform.rotation = temp;
	}
		public void Updatelevel(int level)
		{
			int temp_mod = level % 10;
			int temp_int = level / 100;


			speed = Random.Range(0.5f,2.0f) + (temp_int + temp_mod)/3;
		}

	void OnCollisionEnter2D(Collision2D col)
	{
		GameOverLogic (col.gameObject);
	}

	void OnCollisionStay2D(Collision2D col)
	{
		GameOverLogic (col.gameObject);
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		GameOverLogic (col.gameObject);
	}

	void OnTriggerStay2D(Collider2D col) 
	{
		GameOverLogic (col.gameObject);
	}

	void GameOverLogic(GameObject col)
	{
		if( !gameManager.isGameOver &&  col.name.Contains("Dot"))
		{
			GetComponent<Collider2D> ().enabled = false;

			col.GetComponent<Collider2D> ().enabled = false;

			gameManager.AnimationCameraGameOver ();
		}

	}
}
}