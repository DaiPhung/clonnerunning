﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AppAdvisory.AA
{
public class ResizeSpriteToScreen : MonoBehaviour {

	// Use this for initialization
	void Start () {
			SpriteRenderer sr = GetComponent <SpriteRenderer> ();
			if (sr == null)
				return;
			transform.localScale = new Vector3 (1,1,1);
			float width = sr.sprite.bounds.size.x;
			float heightscreen = 2f * Camera.main.orthographicSize;
			float widthscreen = heightscreen * Camera.main.aspect;

			Vector3 xWidth = transform.localScale;
			xWidth.x = widthscreen / width;
			xWidth.y = xWidth.x;
			transform.localScale = xWidth;
	//		Vector3 yHeight = transform.localScale;
	//		yHeight.y = heightscreen / height;
	//		transform.localScale = yHeight;
		
	}
		void Awake()
		{

		}
	
	// Update is called once per frame
	void Update () {
		
	}
}
}