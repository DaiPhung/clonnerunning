
/***********************************************************************************************************
 * Produced by App Advisory	- http://app-advisory.com													   *
 * Facebook: https://facebook.com/appadvisory															   *
 * Contact us: https://appadvisory.zendesk.com/hc/en-us/requests/new									   *
 * App Advisory Unity Asset Store catalog: http://u3d.as/9cs											   *
 * Developed by Gilbert Anthony Barouch - https://www.linkedin.com/in/ganbarouch                           *
 ***********************************************************************************************************/

using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;


#if AADOTWEEN


namespace AppAdvisory.AA
{
	/// <summary>
	/// Class in charge to prompt a popup to ask the player to rate the game
	///
	/// Attached to the "RateUsManager" GameObject
	/// </summary>
	public class PromptStars : MonoBehaviour 
	{
		/// <summary>
		/// The number of level played (each win or lose count for 1 play) to prompt the popup
		/// </summary>
		public Button btnYes;

		public CanvasGroup popupCanvasGroup;

		int lastLevel
		{
			get 
			{
				return PlayerPrefs.GetInt (Constant.LAST_LEVEL_PLAYED, 1);
			}
		}

		void Awake()
		{
			popupCanvasGroup.alpha = 0;
			popupCanvasGroup.gameObject.SetActive(false);
		}

		void AddButtonListeners()
		{
			btnYes.onClick.AddListener(OnClickedYes);
		}

		void RemoveButtonListener()
		{
			btnYes.onClick.RemoveListener(OnClickedYes);
		}
		/// <summary>
		/// Method called if the player clicked on the YES button. If the player do that, we will never prompt again the popup
		/// </summary>
		void OnClickedYes()
		{
			HidePopup();
		}

		/// <summary>
		/// Method to prompt the popup
		/// </summary>
		public void PromptPopup()
		{
			FindObjectOfType<InputTouch>().BLOCK_INPUT = true;

			popupCanvasGroup.alpha = 0;
			popupCanvasGroup.gameObject.SetActive(true);

			StartCoroutine(DoLerpAlpha(popupCanvasGroup, 0, 1, 1, () => {
				AddButtonListeners();
			}));
		}
		/// <summary>
		/// Method to hide the popup
		/// </summary>
		void HidePopup()
		{
			StartCoroutine(DoLerpAlpha(popupCanvasGroup, 1, 0, 1, () => {
				popupCanvasGroup.gameObject.SetActive(false);
				RemoveButtonListener();
				FindObjectOfType<InputTouch>().BLOCK_INPUT = false;

			}));
		}

		public IEnumerator DoLerpAlpha(CanvasGroup c, float from, float to, float time, Action callback)
		{
			float timer = 0;

			c.alpha = from;

			while (timer <= time)
			{
				timer += Time.deltaTime;
				c.alpha = Mathf.Lerp(from, to, timer / time);
				yield return null;
			}

			c.alpha = to;

			if (callback != null)
				callback ();
		}
	}
}

#endif