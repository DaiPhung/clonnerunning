﻿
/***********************************************************************************************************
 * Produced by App Advisory	- http://app-advisory.com													   *
 * Facebook: https://facebook.com/appadvisory															   *
 * Contact us: https://appadvisory.zendesk.com/hc/en-us/requests/new									   *
 * App Advisory Unity Asset Store catalog: http://u3d.as/9cs											   *
 * Developed by Gilbert Anthony Barouch - https://www.linkedin.com/in/ganbarouch                           *
 ***********************************************************************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
#if AADOTWEEN
using DG.Tweening;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace AppAdvisory.AA
{
	/// <summary>
	/// Class who make the level. Click right on the LevelManager in the editor and select "execute" to generate 600 levels.
	/// </summary>
	public class LevelManager : MonobehaviourHelper
	{
		public List<Level> levels;

		void Awake(){
			int level = PlayerPrefs.GetInt ("LEVEL_PLAYED");
			if (level > 600) {
				PlayerPrefs.SetInt ("LEVEL_PLAYED", 600);
			}
		}

		#if UNITY_EDITOR
		[ContextMenu("Execute")]
		public virtual void CreateLevels ()
		{
			Debug.Log("execute");


			levels = new List<Level> ();

			for (int i = 1; i <= 600; i++) 
			{
				Level l = new Level (i);
				if (i % 2 == 0) {
					l.rotateRight = true;
				} else {
					l.rotateRight = false;
				}
				levels.Add (l);
			}
		}
		#endif

		public Level GetLevel(int level)
		{
			return levels [level - 1];

		}
	}


	/// <summary>
	/// Level class with all the informations for to create the level in the GameManager
	/// </summary>
	[Serializable]
	public class Level
	{

		public int levelNumber = 0;
		public int numberDotsToCreate = 0;
		public int numberDotsOnCircle = 0;
		public float timeleft = 60;
		public float timecombo = 0.8f;
		/// <summary>
		/// Radius of the line who link the dots to the circle
		/// </summary>
		[Range(0.7f,1f)] public float lineRadius = 1f;

		/// <summary>
		/// Delay of one rotation
		/// </summary>
		public float rotateDelay = 8f;

		/// <summary>
		/// Define the direction of the rotation
		/// </summary>
		public bool rotateRight;

		/// <summary>
		/// Define the direction of the rotation
		/// </summary>
		[HideInInspector] 
		public Vector3 rotateVector
		{
			get
			{ 
				if (rotateRight)
					return new Vector3 (0, 0, 1);
				else
					return new Vector3 (0, 0, -1);
			}
		}

		/// <summary>
		/// Ease type of the rotation
		/// </summary>
		public Ease rotateEase = Ease.InCirc;

		/// <summary>
		/// Loop type of the rotation
		/// </summary>
		public LoopType rotateLoopType = LoopType.Incremental;

		public override string ToString()
		{
			return "levelNumber = " + levelNumber + " - numberDotsToCreate = " + numberDotsToCreate + " - numberDotsOnCircle = " + numberDotsOnCircle + " - lineRadius = " + lineRadius + " - rotateDelay = " + rotateDelay
				+ " - rotateVector = " + rotateVector + " - rotateEase " + rotateEase.ToString () + " - rotateLoopType = " + rotateLoopType.ToString ();
		}

		/// <summary>
		/// Don't create more than 600 levels
		/// </summary>
		static int maxLevel = 600;


		/// <summary>
		/// Level constructor
		/// </summary>
		public Level (int level){

			levelNumber = level;


			if (level == 1) {
				numberDotsToCreate = 5;
				numberDotsOnCircle = 0;

			} else if (level == 2) {
				numberDotsToCreate = 7;
				numberDotsOnCircle = 1;

			} else if (level == 3) {
				numberDotsToCreate = 10;
				numberDotsOnCircle = 2;

			} else if (level == 4) {
				numberDotsToCreate = 7;
				numberDotsOnCircle = 3;

			} else if (level == 5) {
				numberDotsToCreate = 6;
				numberDotsOnCircle = 4;

			} else if (level > 5 && level <=50){


				numberDotsToCreate = 
					(int)(
						(5 + level % 10)
					);

				numberDotsOnCircle = 
					(int)(
						(15 -  numberDotsToCreate)
					);

			} else if (level > 50 && level <=150){


				numberDotsToCreate = 
					(int)(
						(6 + level % 10)
					);

				numberDotsOnCircle = 
					(int)(
						(17 -  numberDotsToCreate)
					);

			} else if (level > 150 && level <=250){


				numberDotsToCreate = 
					(int)(
						(7 + level % 10)
					);

				numberDotsOnCircle = 
					(int)(
						(19 -  numberDotsToCreate)
					);

			} else if (level > 250 && level <=350){


				numberDotsToCreate = 
					(int)(
						(7 + level % 10)
					);

				numberDotsOnCircle = 
					(int)(
						(20 -  numberDotsToCreate)
					);

			}else if (level > 350 && level <=500){


				numberDotsToCreate = 
					(int)(
						(8 + level % 10)
					);

				numberDotsOnCircle = 
					(int)(
						(21 -  numberDotsToCreate)
					);

			}else if (level > 500){


				numberDotsToCreate = 
					(int)(
						(10 + level % 10)
					);

				numberDotsOnCircle = 
					(int)(
						(23 -  numberDotsToCreate)
					);

			}

			Debug.Log ("level :" + level + "numberDotsToCreate :" + numberDotsToCreate + "numberDotsOnCircle :" +numberDotsOnCircle);
			if (level > 5) {
				lineRadius = 1f - (level % 5f) / 10f;
			}




			if (level > 10) {

				if (level % 3 < 1) {
					lineRadius = 1f - (level % 5 + 1) / 10f;
				}

			}



			if (level > 20 && level < 30) {

				if (level % 2 == 1) {
					rotateDelay = 8f - (level % 3);
				} else {
					rotateDelay = 8f + (level % 3);
				}

			} else {

				if (level % 2 == 1) {
					rotateDelay = 10f - (level % 3);
				} else {
					rotateDelay = 10f + (level % 3);
				}
			}
			/*
			int variable = 0;
			if (level <= maxLevel/2) {

				rotateLoopType = LoopType.Incremental;

				variable = 0;
			} else {

				rotateLoopType = LoopType.Yoyo;
				variable = maxLevel/2;
			}
			*/
			if (level <= 50) {
				rotateLoopType = LoopType.Incremental;
			} else if (level > 50 && level <= 150) {
				if (UnityEngine.Random.value < 0.5f) {
					rotateLoopType = LoopType.Incremental;
				} else {
					rotateLoopType = LoopType.Yoyo;
				}
			} else if (level > 150 && level <= 250) {
				if (UnityEngine.Random.value < 0.4f) {
					rotateLoopType = LoopType.Incremental;
				} else {
					rotateLoopType = LoopType.Yoyo;
				}
			} else if (level > 250 && level <= 350) {
				timeleft = 70;
				if (UnityEngine.Random.value < 0.3f) {
					rotateLoopType = LoopType.Incremental;
				} else {
					rotateLoopType = LoopType.Yoyo;
				}
			} else if (level > 350 && level <= 500) {
				timeleft = 80;
				if (UnityEngine.Random.value < 0.2f) {
					rotateLoopType = LoopType.Incremental;
				} else {
					rotateLoopType = LoopType.Yoyo;
				}
			} else {
				timeleft = 90;
				if (UnityEngine.Random.value < 0.1f) {
					rotateLoopType = LoopType.Incremental;
				}else {
					rotateLoopType = LoopType.Yoyo;
				}
			}
			/*
			if (level < 30) {
				//			Debug.Log ("level = " + level + " Ease.linear");
				rotateEase = Ease.Linear;
			}else if (level < 50) {
				//			Debug.Log ("level = " + level + " Ease.spring");
				rotateEase = Ease.Linear;
			} else if (level < 70 ) {
				rotateEase = Ease.InQuad;
			} else if (level < 90 ) {
				rotateEase = Ease.OutCirc;
			} else if (level < 110 ) {
				rotateEase = Ease.InOutQuart;
			} else if (level < 130 ) {
				rotateEase = Ease.InQuint;
			} else if (level < 150 ) {
				rotateEase = Ease.OutSine;
			} else if (level < 170 ) {
				rotateEase = Ease.InOutExpo;
			} else if (level < 190 ) {
				rotateEase = Ease.InCirc;
			} else if (level < 210 ) {
				rotateEase = Ease.OutBounce;
			} else if (level < 230 ) {
				rotateEase = Ease.InOutQuint;
			} else if (level < 250 ) {
				rotateEase = Ease.InExpo;
			} else if (level < 270 ) {
				rotateEase = Ease.OutQuart;
			} else if (level < 290 ) {
				rotateEase = Ease.InOutQuad;
			} else if (level < 310 ) {
				rotateEase = Ease.InSine;
			} else if (level < 330 ) {
				rotateEase = Ease.OutExpo;
			} else if (level < 350 ) {
				rotateEase = Ease.InOutCubic;
			} else if (level < 370 ) {
				rotateEase = Ease.InBounce;
			} else if (level < 390 ) {
				rotateEase = Ease.OutQuint;
			} else if (level < 410 ) {
				rotateEase = Ease.InOutSine;
			} else if (level < 430 ) {
				rotateEase = Ease.InQuart;
			} else if (level < 450 ) {
				rotateEase = Ease.OutQuad;
			} else if (level < 470 ) {
				rotateEase = Ease.InOutCirc;
			} else if (level < 490 ) {
				rotateEase = Ease.InCubic;
			} else if (level < 510 ) {
				rotateEase = Ease.OutCubic;
			} else if (level < 530 ) {
				rotateEase = Ease.InOutBounce;
			}
*/
			int numOfEnum = (System.Enum.GetValues (typeof(Ease)).Length);


	//		int enumNumber = level % numOfEnum;
	//		rotateEase = (Ease)(enumNumber);
			int enumNumber = numOfEnum/6;
			if (level <= 50) {
			} else if (level > 50 && level <= 150) {
				enumNumber = enumNumber * 2;
			} else if (level > 150 && level <= 250) {
				enumNumber = enumNumber * 3;
			} else if (level > 250 && level <= 350) {
				enumNumber = enumNumber * 4;
			} else if (level > 350 && level <= 500) {
				enumNumber = enumNumber * 5;
			} else {
				enumNumber = numOfEnum;
			}
			rotateEase = (Ease)(UnityEngine.Random.Range(0,enumNumber));
			while(rotateEase.ToString ().Contains ("Elastic") || rotateEase.ToString ().Contains ("INTERNAL_Zero") || rotateEase.ToString ().Contains ("INTERNAL_Custom"))
			{
				enumNumber++;
				if (enumNumber >= numOfEnum)
					enumNumber = 0;

				rotateEase = (Ease)(enumNumber);
			}

			if (level > maxLevel) {
				PlayerPrefs.SetInt (Constant.LEVEL_UNLOCKED, 600);

				level = 600;
				PlayerPrefs.SetInt (Constant.LAST_LEVEL_PLAYED, 600);

				Application.OpenURL ("http://barouch.fr/moregames.php");

			}

		}


	}
}


#endif