﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


#if AADOTWEEN


namespace AppAdvisory.AA
{
	/// <summary>
	/// Class in charge to prompt a popup to ask the player to rate the game
	///
	/// Attached to the "RateUsManager" GameObject
	/// </summary>
public class SoundSetting : MonoBehaviour {


		public Button btnOkie;
		public Button btnSoundSFX;
		public Button btnSoundBGM;
		public CanvasGroup popupCanvasGroup;

		AudioSource _music;
		/// <summary>
		/// Audiosource with the music attached (if you add a music to it)
		/// </summary>
		AudioSource music
		{
			get 
			{
				if (_music == null)
					_music = Camera.main.GetComponentInChildren<AudioSource> ();

				return _music;
			}
		}
	// Use this for initialization

	void Awake()
	{
		popupCanvasGroup.alpha = 0;
		popupCanvasGroup.blocksRaycasts = false;
		popupCanvasGroup.gameObject.SetActive(false);
		CheckFSX ();

	}
	
	void Start () {

		Button btnokie = btnOkie.GetComponent<Button> ();
		btnokie.onClick.AddListener (OnClickedClosed);

		Button btnsoundFSX = btnSoundSFX.GetComponent<Button> ();
		btnsoundFSX.onClick.AddListener (OnClickedFSX);

		Button btnsoundBGM = btnSoundBGM.GetComponent<Button> ();
		btnsoundBGM.onClick.AddListener (OnClickedBGM);
		CheckBGM ();
		}
	
	void CheckFSX(){
			int FSXOn = PlayerPrefs.GetInt (Constant.SOUNDFSX_ON, 1);
			Color c = btnSoundSFX.GetComponent<Image> ().color;
			if (FSXOn == 0) {
				btnSoundSFX.GetComponent<Image> ().color = new Color(c.r,c.g,c.b,0.5f);

			} 
	  }

	void CheckBGM(){
			int BGMOn = PlayerPrefs.GetInt (Constant.SOUNDBGM_ON, 1);
			Color c = btnSoundBGM.GetComponent<Image> ().color;
			if (BGMOn == 0) {
				music.Stop ();
				btnSoundBGM.GetComponent<Image> ().color = new Color (c.r, c.g, c.b, 0.5f);
			} else {
				music.Play();
			}
		}

	void OnClickedFSX(){
			int FSXOn = PlayerPrefs.GetInt (Constant.SOUNDFSX_ON, 1);
			Color c = btnSoundSFX.GetComponent<Image> ().color;
			if (FSXOn == 1) {
				btnSoundSFX.GetComponent<Image> ().color = new Color(c.r,c.g,c.b,0.5f);
				PlayerPrefs.SetInt (Constant.SOUNDFSX_ON, 0);
			} else {
				btnSoundSFX.GetComponent<Image> ().color = new Color(c.r,c.g,c.b,1f);
				PlayerPrefs.SetInt (Constant.SOUNDFSX_ON, 1);
			}
		}

	void OnClickedBGM(){
			int BGMOn = PlayerPrefs.GetInt(Constant.SOUNDBGM_ON,1);
			Color c = btnSoundBGM.GetComponent<Image> ().color;
			if (BGMOn == 1) {
				music.Stop ();
				btnSoundBGM.GetComponent<Image> ().color = new Color(c.r,c.g,c.b,0.5f);
				PlayerPrefs.SetInt (Constant.SOUNDBGM_ON, 0);
			} else {
				music.Play ();
				btnSoundBGM.GetComponent<Image> ().color = new Color(c.r,c.g,c.b,1f);
				PlayerPrefs.SetInt (Constant.SOUNDBGM_ON, 1);
			}
	}

	void OnClickedClosed()
	{
		popupCanvasGroup.alpha = 0;
		popupCanvasGroup.blocksRaycasts = false;
	}
	// Update is called once per frame
	void Update () {
		
	}

	public void PromptPopup()
	{
		GetComponent<Canvas> ().sortingOrder = 111;
		popupCanvasGroup.gameObject.SetActive(true);
		popupCanvasGroup.alpha = 1;
		popupCanvasGroup.blocksRaycasts = true;
	}
}
}
#endif